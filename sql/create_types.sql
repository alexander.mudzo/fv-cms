-- Create dml_type enum if not exists
DO
'
BEGIN
  IF NOT EXISTS (SELECT *
                        FROM pg_type typ
                             INNER JOIN pg_namespace nsp
                                        ON nsp.oid = typ.typnamespace
                        WHERE nsp.nspname = current_schema()
                              AND typ.typname = ''dml_type'') THEN
    CREATE TYPE dml_type AS ENUM (''INSERT'', ''UPDATE'', ''DELETE'');
  END IF;
END;
'
LANGUAGE plpgsql;