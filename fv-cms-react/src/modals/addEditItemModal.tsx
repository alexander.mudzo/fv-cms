import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { MenuItem, TextField } from '@mui/material';
import AddEditItemCategoryModal from './addEditItemCategoryModal';
import WarehouseItem from '../types/warehouseItem.type';
import Category from '../types/category.type';
import WarehouseService from '../services/rest/warehouse.service';
import { Delete } from '@mui/icons-material';

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  textAlign: 'start'
};

type OnConfirmedCallback = (item?: WarehouseItem) => void;

type Props = {
  item?: WarehouseItem;
  categories: readonly Category[];
  onCofirmed?: OnConfirmedCallback;
}


const AddEditItemModal: React.FC<Props> = (props: Props) => {

  const [open, setOpen] = React.useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false)
  }


  const [categories, setCategories] = React.useState<Category[]>([]);

  const [category, setCategory] = React.useState<Category>((props.item?.categories !== undefined && props.item!.categories!.length > 0)
    ? props.item!.categories[0] : {});

  React.useEffect(() => {
    setCategories([...props.categories])
    setCount(props.item?.count ?? 0)
    setName(props.item?.name ?? "")
  }, [props, open])

  const [count, setCount] = React.useState<number>(props.item?.count ?? 0);

  const [name, setName] = React.useState<string>(props.item?.name ?? "");

  const handleCategoryChange = (event: React.ChangeEvent<HTMLInputElement>) => {

    const cat = categories.find((c) => c.id! === parseInt(event.target.value));
    setCategory(cat!);
  };

  const handleNameInputChange = (value: string) => {
    setName(value);
  };

  const handleCountInputChange = (value: string) => {
    const val = parseInt(value)
    if (val >= 0)
      setCount(val);
  };

  const handleSave = () => {
    if (props.item) {
      WarehouseService.updateItem({
        name: name,
        count: count,
        categories: [
          {
            id: category?.id,
          }
        ]
      }, props.item!.id!).then((r) => {
        props.onCofirmed?.call(r.data)
        handleClose()
      })
    }
    else {
      WarehouseService.createNewItem({
        name: name,
        count: count,
        categories: [
          {
            id: category?.id,
          }
        ]
      }).then((r) => {
        props.onCofirmed?.call(r.data)
        handleClose()
      })
    }
  };

  const handleDelete = () => {
    WarehouseService.deleteItem(props.item!.id!).then((r) => {
      props.onCofirmed?.call(this)
      handleClose()
    })
  };

  const isCreate = props.item == null

  const handleDialogClick = (e: React.MouseEvent) => {
    e.stopPropagation();
  };


  const handleCategoryAdded = (cat: Category) => {
    console.log(cat.id)
    const cats = [cat, ...categories];
    setCategories(cats);
  }

  return (

    <div>{isCreate
      ?
      <Button onClick={handleOpen}>Create new item</Button>
      : <Button onClick={handleOpen}>Edit</Button>
    }
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        onClick={handleDialogClick}
      >
        <Box sx={{ ...style, '& .MuiTextField-root': { m: 1, width: '25ch' } }}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            {isCreate ? 'Create new item' : 'Edit item'}
          </Typography>

          <TextField
            value={name}
            required
            id="outlined-basic"
            label="Name"
            variant="outlined"
            fullWidth
            helperText="Enter item name"
            onChange={(e) => handleNameInputChange(e.target.value)}
          />
          <TextField
            value={count}
            required
            id="outlined-basic"
            label="Count"
            variant="outlined"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            helperText="Enter count of items"
            onChange={(e) => handleCountInputChange(e.target.value)}
          />

          <div className="row-start" >

            <TextField
              id="outlined-select-currency"
              select
              label="Category"
              value={category.id}
              onChange={handleCategoryChange}
              helperText="Select Category"
            >
              {categories.map((option) => (
                <MenuItem key={option.id!} value={option.id!}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
            <AddEditItemCategoryModal
              onCreated={handleCategoryAdded}
            />

          </div>


          <div className="row" >
            {!isCreate && <Button
              variant="outlined"
              startIcon={<Delete />}
              color="error"
              onClick={handleDelete}
            >
              Delete
            </Button>}
            <Button
              variant="contained"
              color="secondary"
              onClick={handleClose}
            >Cancel</Button>
            <Button
              variant="contained"
              color="success"
              onClick={handleSave}
            >Save</Button>

          </div>

        </Box>
      </Modal>
    </div >

  );
}

export default AddEditItemModal