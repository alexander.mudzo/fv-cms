import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import { IconButton, TextField, Typography } from '@mui/material';
import { Add } from '@mui/icons-material';
import CategoriesService from '../services/rest/categories.service';

import Category from '../types/category.type';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
};
type OnCreatedCallback = (item: Category) => void;


type Props = {
    onCreated: OnCreatedCallback,
}



const handleDialogClick = (e: React.MouseEvent) => {
    e.stopPropagation();
};

const AddEditItemCategoryModal: React.FC<Props> = (props: Props) => {

    const [open, setOpen] = React.useState(false);
    const [name, setName] = React.useState("");

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setName("")
    };

    const handleSave = () => {

        CategoriesService.createNewItem({
            name: name,
        }).then((r) => {
            props.onCreated(r.data);
            handleClose()
        })

    };

    const handleNameInputChange = (value: string) => {
        setName(value);
    };

    return (
        <React.Fragment>
            <IconButton
                aria-label="add"
                onClick={handleOpen}
            >
                <Add sx={{ fontSize: "50px" }} />
            </IconButton>
            <Modal

                hideBackdrop
                open={open}
                onClose={handleClose}
                aria-labelledby="child-modal-title"
                aria-describedby="child-modal-description"
                onClick={handleDialogClick}
            >
                <Box sx={{ ...style, width: 300 }}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Create category
                    </Typography>

                    <TextField
                        required
                        id="outlined-basic"
                        label="Name"
                        variant="outlined"
                        fullWidth
                        helperText="Enter category name"
                        onChange={(event) => handleNameInputChange(event.target.value)}
                    />
                    <div className="row" >
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={handleClose}
                        >Cancel</Button>

                        <Button
                            variant="contained"
                            color="success"
                            onClick={handleSave}
                        >Save</Button>

                    </div>
                </Box>
            </Modal>
        </React.Fragment>
    );
}

export default AddEditItemCategoryModal;