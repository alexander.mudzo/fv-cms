import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Checkbox, FormControl, InputLabel, ListItemText, MenuItem, OutlinedInput, Paper, Select, SelectChangeEvent, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from '@mui/material';
import WarehouseItem from '../types/warehouseItem.type';
import { Delete } from '@mui/icons-material';
import OrderItem from '../types/order.type';
import orderService from '../services/rest/order.service';
import Supplier from '../types/supplier';
import { formatCategoriesList } from '../utils/formattingUtils';
import { MenuProps } from '../common/consts';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    textAlign: 'start'
};

type OnConfirmedCallback = (order?: OrderItem) => void;

type Props = {
    order?: OrderItem;
    items: readonly WarehouseItem[];
    suppliers: readonly Supplier[];
    onCofirmed: OnConfirmedCallback;
}

const AddEditOrderModal: React.FC<Props> = (props: Props) => {

    const [open, setOpen] = React.useState(false);

    const handleOpen = () => setOpen(true);

    const handleClose = () => setOpen(false);


    const [items, setItems] = React.useState<WarehouseItem[]>([...props.items]);

    const [suppliers, setSuppliers] = React.useState<Supplier[]>([...props.suppliers]);

    const [selectedItemIds, setSelectedItemIds] = React.useState<string[]>(props.order?.items!.map((i) => i.item?.id!) ?? []);

    const [selectedSupplier, setSelectedSupplier] = React.useState<Supplier>(props.order?.supplier ?? {});

    const [itemNeededCount, setItemNeededCount] = React.useState<Map<string, number>>(new Map(
        props.order?.items!.map((itm) => [itm.item!.id!, itm.count!]) ?? []
    ));


    React.useEffect(() => {
        setSuppliers([...props.suppliers])
        setItems([...props.items])
        setSelectedItemIds(props.order?.items!.map((i) => i.item?.id!) ?? [])
        setSelectedSupplier(props.order?.supplier ?? {})
        setItemNeededCount(new Map(
            props.order?.items!.map((itm) => [itm.item!.id!, itm.count!]) ?? []
        ))
    }, [props, open])

    const handleSupplierChange = (event: React.ChangeEvent<HTMLInputElement>) => {

        const sup = suppliers.find((c) => c.id! === event.target.value);
        setSelectedSupplier(sup!);
    };

    const handleSave = () => {
        if (props.order) {
            orderService.updateItem({
                items: Array.from(itemNeededCount.entries()).map(([k, v]) =>
                ({
                    item: { id: k },
                    count: v,
                })
                ),
                supplier: selectedSupplier,
            }, props.order!.id!).then((r) => {
                props.onCofirmed?.call<OrderItem, any, void>(r.data)
                handleClose()
            })
        }
        else {
            orderService.createNewItem({
                items: Array.from(itemNeededCount.entries()).map(([k, v]) =>
                ({
                    item: { id: k },
                    count: v,
                })
                ),
                supplier: selectedSupplier,
            }).then((r) => {
                props.onCofirmed(r.data)
                handleClose()
            })
        }
    };

    const handleDelete = () => {
        orderService.deleteItem(props.order!.id!).then((r) => {
            props.onCofirmed(undefined)
            handleClose()
        })
    };

    const isCreate = props.order == null

    const handleDialogClick = (e: React.MouseEvent) => {
        e.stopPropagation();
    };

    const handleChange = (event: SelectChangeEvent<typeof selectedItemIds>) => {
        const {
            target: { value },
        } = event;
        setSelectedItemIds(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );


        let newMap = new Map()
        selectedItemIds.forEach(e => {
            if (itemNeededCount.has(e)) {
                newMap.set(e, itemNeededCount.get(e));
            }
        });
        setItemNeededCount(newMap)

    };

    const handleCountInputChange = (value: string, id: string) => {

        const val = parseInt(value)

        if (val >= 0) setItemNeededCount(new Map(itemNeededCount.set(id, val)))
    };

    return (

        <div>{isCreate
            ?
            <Button onClick={handleOpen}>Create order</Button>
            : <Button onClick={handleOpen}>Edit</Button>
        }
            <Modal

                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                onClick={handleDialogClick}
            >
                <Box sx={{ ...style, '& .MuiTextField-root': { m: 1, width: '25ch' } }}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {isCreate ? 'Create order' : 'Edit order'}
                    </Typography>

                    <TextField
                        id="outlined-select-currency"
                        select
                        label="Supplier"
                        value={selectedSupplier.id}
                        onChange={handleSupplierChange}
                        helperText="Select supplier"
                    >
                        {suppliers.map((supplier) => (
                            <MenuItem key={supplier.id!} value={supplier.id!}>
                                {supplier.name}
                            </MenuItem>
                        ))}
                    </TextField>

                    <FormControl sx={{ m: 1, width: 300 }}>
                        <InputLabel>Items</InputLabel>
                        <Select
                            multiple
                            value={selectedItemIds}
                            onChange={handleChange}
                            input={<OutlinedInput label="Item" />}
                            renderValue={(_) => items.filter((f) => selectedItemIds.indexOf(f.id!) > -1).map((f) => f.name!).join(', ')}
                            MenuProps={MenuProps}
                        >
                            {items.map((item) => (
                                <MenuItem key={item.id!} value={item.id!}>
                                    <Checkbox checked={selectedItemIds.indexOf(item.id!) > -1} />
                                    <ListItemText primary={item.name} />
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <div style={{ paddingBottom: "50px" }}>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 300 }} size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Selected Item</TableCell>
                                        <TableCell >Available</TableCell>
                                        <TableCell align="right">Categories</TableCell>
                                        <TableCell align="right"></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {items.filter((e) => selectedItemIds.indexOf(e.id!) > -1).map((row) => (
                                        <TableRow
                                            key={row.name}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell component="th" scope="row">
                                                {row.name}
                                            </TableCell>
                                            <TableCell >{row.count}</TableCell>
                                            <TableCell align="right">{formatCategoriesList(row.categories ?? [])}</TableCell>
                                            <TableCell align="right">
                                                <TextField
                                                    style={{ width: 80 }}
                                                    value={itemNeededCount.get(row.id!) ?? 0}
                                                    required
                                                    id="outlined-basic"
                                                    variant="outlined"
                                                    label={"Required"}
                                                    inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                                                    onChange={(e) => handleCountInputChange(e.target.value, row.id!)}
                                                />
                                            </TableCell>


                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>

                    <div className="row" >
                        {!isCreate && <Button
                            variant="outlined"
                            startIcon={<Delete />}
                            color="error"
                            onClick={handleDelete}
                        >
                            Delete
                        </Button>}
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={handleClose}
                        >Cancel</Button>
                        <Button
                            variant="contained"
                            color="success"
                            onClick={handleSave}
                        >Save</Button>

                    </div>

                </Box>
            </Modal>
        </div >

    );
}

export default AddEditOrderModal