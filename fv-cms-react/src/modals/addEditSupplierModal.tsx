import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { TextField } from '@mui/material';
import { Delete } from '@mui/icons-material';
import SupplierService from '../services/rest/supplier.service';
import Supplier from '../types/supplier';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    textAlign: 'start'
};

type OnConfirmedCallback = (supplier?: Supplier) => void;

type Props = {
    supplier?: Supplier;
    onCofirmed: OnConfirmedCallback;
}

const AddEditSupplierModal: React.FC<Props> = (props: Props) => {

    const [open, setOpen] = React.useState(false);

    const [name, setName] = React.useState(props.supplier?.name ?? '');

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    const handleNameInputChange = (value: string) => {
        setName(value);
    };

    React.useEffect(() => {
        setName(props.supplier?.name ?? '')
    }, [props, open])


    const handleSave = () => {

        if (props.supplier) {
            SupplierService.updateItem({
                name: name
            }, props.supplier!.id!).then((r) => {
                props.onCofirmed(r.data)
                handleClose()
            })
        }
        else {
            SupplierService.createNewItem({
                name: name,
            }).then((r) => {
                props.onCofirmed(r.data)
                handleClose()
            })
        }
    };

    const handleDelete = () => {
        SupplierService.deleteItem(props.supplier!.id!).then((r) => {
            props.onCofirmed?.call(this)
            handleClose()
        })
    };

    const isCreate = props.supplier == null

    const handleDialogClick = (e: React.MouseEvent) => {
        e.stopPropagation();
    };

    return (

        <div>{isCreate
            ?
            <Button onClick={handleOpen}>Add new supplier</Button>
            : <Button onClick={handleOpen}>Edit</Button>
        }
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                onClick={handleDialogClick}
            >
                <Box sx={{ ...style, '& .MuiTextField-root': { m: 1, width: '25ch' } }}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {isCreate ? 'Add new supplier' : 'Edit supplier'}
                    </Typography>

                    <TextField
                        value={name}
                        required
                        id="outlined-basic"
                        label="Name"
                        variant="outlined"
                        fullWidth
                        helperText="Enter supplier name"
                        onChange={(e) => handleNameInputChange(e.target.value)}
                    />

                    <div className="row" >
                        {!isCreate && <Button
                            variant="outlined"
                            startIcon={<Delete />}
                            color="error"
                            onClick={handleDelete}
                        >
                            Delete
                        </Button>}
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={handleClose}
                        >Cancel</Button>
                        <Button
                            variant="contained"
                            color="success"
                            onClick={handleSave}
                        >Save</Button>

                    </div>

                </Box>
            </Modal>
        </div >

    );
}

export default AddEditSupplierModal