import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Checkbox, FormControl, InputLabel, ListItemText, MenuItem, OutlinedInput, Paper, Select, SelectChangeEvent, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from '@mui/material';
import WarehouseItem from '../types/warehouseItem.type';
import { Delete } from '@mui/icons-material';
import orderService from '../services/rest/order.service';
import ExportJob from '../types/exportJob';
import ExportJobService from '../services/rest/exportJob.service';
import { formatCategoriesList } from '../utils/formattingUtils';
import { MenuProps } from '../common/consts';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    textAlign: 'start'
};

type OnConfirmedCallback = (job?: ExportJob) => void;

type Props = {
    job?: ExportJob;
    onCofirmed: OnConfirmedCallback;
    items: readonly WarehouseItem[];
}


const AddEditExportJobModal: React.FC<Props> = (props: Props) => {

    const [open, setOpen] = React.useState(false);

    const handleOpen = () => setOpen(true);
    const handleClose = () => {
        setOpen(false)
    };

    const [items, setItems] = React.useState<WarehouseItem[]>([...props.items]);

    const [name, setName] = React.useState(props.job?.name ?? "");

    const [description, setDescription] = React.useState(props.job?.description ?? "");

    const [selectedItemIds, setSelectedItemIds] = React.useState<string[]>(props.job?.items!.map((i) => i.item?.id!) ?? []);

    const [itemNeededCount, setItemNeededCount] = React.useState<Map<string, number>>(new Map(
        props.job?.items!.map((itm) => [itm.item!.id!, itm.count!]) ?? []
    ));

    const [missingItemNeededCount, setMissingItemNeededCount] = React.useState<Map<string, number>>(new Map(
        props.job?.items!.map((itm) => [itm.item!.id!, itm.missingCount!]) ?? []
    ));

    const [dateVal, setDateVal] = React.useState<Date | null>(new Date());


    React.useEffect(() => {
        setItems([...props.items])
        setName(props.job?.name ?? "")
        setDescription(props.job?.description ?? "")
        setSelectedItemIds(props.job?.items!.map((i) => i.item?.id!) ?? [])
        setItemNeededCount(new Map(
            props.job?.items!.map((itm) => [itm.item!.id!, itm.count!]) ?? []
        ))
        setMissingItemNeededCount(new Map(
            props.job?.items!.map((itm) => [itm.item!.id!, itm.missingCount!]) ?? []
        ))
    }, [props, open])


    const handleNameInputChange = (value: string) => {
        setName(value);
    }

    const handleDescriptionInputChange = (value: string) => {
        setDescription(value);
    }

    const handleSave = () => {

        if (props.job) {
            ExportJobService.updateItem({
                items: Array.from(itemNeededCount.entries()).map(([k, v]) =>
                ({
                    item: { id: k },
                    count: v,
                })
                ),
                name: name,
                description: description,
                deadline: dateVal?.toLocaleTimeString()
            }, props.job!.id!).then((r) => {
                props.onCofirmed(r.data)
                handleClose()
            })
        }
        else {
            ExportJobService.createNewItem({
                items: Array.from(itemNeededCount.entries()).map(([k, v]) =>
                ({
                    item: { id: k },
                    count: v,
                })
                ),
                name: name,
                description: description,
                deadline: dateVal?.toLocaleTimeString()
            }).then((r) => {
                props.onCofirmed(r.data)
                handleClose()
            })
        }
    };

    const handleDelete = () => {
        orderService.deleteItem(props.job!.id!).then((r) => {
            props.onCofirmed(undefined)
            handleClose()
        })
    };

    const isCreate = props.job == null

    const handleDialogClick = (e: React.MouseEvent) => {
        e.stopPropagation();
    };

    const handleChange = (event: SelectChangeEvent<typeof selectedItemIds>) => {
        const {
            target: { value },
        } = event;
        setSelectedItemIds(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );

        let newMap = new Map()
        selectedItemIds.forEach(e => {
            if (itemNeededCount.has(e)) {
                newMap.set(e, itemNeededCount.get(e));
            }
        });
        setItemNeededCount(newMap)

    };

    const handleCountInputChange = (value: string, id: string) => {

        const val = parseInt(value)

        if (val >= 0) setItemNeededCount(new Map(itemNeededCount.set(id, val)))
    };

    return (

        <div>{isCreate
            ?
            <Button onClick={handleOpen}>Create new job</Button>
            : <Button onClick={handleOpen}>Edit</Button>
        }
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                onClick={handleDialogClick}
            >
                <Box sx={{ ...style, '& .MuiTextField-root': { m: 1, width: '25ch' } }}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {isCreate ? 'Create new item' : 'Edit item'}
                    </Typography>


                    <div>
                        <TextField
                            value={name}
                            required
                            id="outlined-basic"
                            label="Name"
                            variant="outlined"
                            fullWidth
                            helperText="Enter name of order"
                            onChange={(e) => handleNameInputChange(e.target.value)}
                        />

                        <TextField
                            value={description}
                            required
                            id="outlined-basic"
                            label="Description"
                            variant="outlined"
                            fullWidth
                            helperText="Enter item name"
                            onChange={(e) => handleDescriptionInputChange(e.target.value)}
                        />
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DesktopDatePicker
                                label="Deadline"
                                value={dateVal}
                                minDate={new Date('2017-01-01')}
                                onChange={(newValue) => {
                                    setDateVal(newValue);
                                }}
                                renderInput={(params) => <TextField {...params} />}
                            />
                        </LocalizationProvider>
                    </div>
                    <FormControl sx={{ m: 1, width: 300 }}>
                        <InputLabel>Items</InputLabel>
                        <Select
                            multiple
                            value={selectedItemIds}
                            onChange={handleChange}
                            input={<OutlinedInput label="Item" />}
                            renderValue={(_) => items.filter((f) => selectedItemIds.indexOf(f.id!) > -1).map((f) => f.name!).join(', ')}
                            MenuProps={MenuProps}
                        >
                            {items.map((item) => (
                                <MenuItem key={item.id!} value={item.id!}>
                                    <Checkbox checked={selectedItemIds.indexOf(item.id!) > -1} />
                                    <ListItemText primary={item.name} />
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <div style={{ paddingBottom: "50px" }}>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 300 }} size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Selected Item</TableCell>
                                        <TableCell >Available</TableCell>
                                        <TableCell >Missing</TableCell>
                                        <TableCell align="right">Categories</TableCell>
                                        <TableCell align="right"></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {items.filter((e) => selectedItemIds.indexOf(e.id!) > -1).map((row) => (
                                        <TableRow
                                            key={row.name}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell component="th" scope="row">
                                                {row.name}
                                            </TableCell>
                                            <TableCell >{row.count}</TableCell>
                                            <TableCell >{missingItemNeededCount.get(row.id!) ?? 0}</TableCell>
                                            <TableCell align="right">{formatCategoriesList(row.categories ?? [])}</TableCell>
                                            <TableCell align="right">
                                                <TextField
                                                    style={{ width: 80 }}
                                                    value={itemNeededCount.get(row.id!) ?? 0}
                                                    required
                                                    id="outlined-basic"
                                                    variant="outlined"
                                                    label={"Required"}
                                                    inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                                                    onChange={(e) => handleCountInputChange(e.target.value, row.id!)}
                                                />
                                            </TableCell>


                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>

                    <div className="row" >
                        {!isCreate && <Button
                            variant="outlined"
                            startIcon={<Delete />}
                            color="error"
                            onClick={handleDelete}
                        >
                            Delete
                        </Button>}
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={handleClose}
                        >Cancel</Button>
                        <Button
                            variant="contained"
                            color="success"
                            onClick={handleSave}
                        >Save</Button>

                    </div>

                </Box>
            </Modal>
        </div >

    );
}

export default AddEditExportJobModal