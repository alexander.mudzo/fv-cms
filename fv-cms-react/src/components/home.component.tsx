import { Component } from "react";
import authService from "../services/rest/auth.service";
import AlignItemsList from "./orderAudit.component";

type Props = {};

type State = {
  content: string;
  authenticated: boolean;
}

export default class Home extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      authenticated: false,
      content: ""
    };
  }

  componentDidMount() {

    const user = authService.getCurrentUser();

    this.setState({
      content: user != null ? `Welcome ${user.userName}` : 'Placeholder for public content',
      authenticated: user != null
    });
  }

  render() {
    return (
      <div className="container\">
        <header className="jumbotron">
          <h3>{this.state.content}</h3>
        </header>

        {this.state.authenticated && <AlignItemsList></AlignItemsList>}


      </div>
    );
  }
}