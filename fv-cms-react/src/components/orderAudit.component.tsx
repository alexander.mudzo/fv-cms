import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import OrderAudit from '../types/orderAudit';
import { useEffect, useState } from 'react';
import orderService from '../services/rest/order.service';
import { formatDatabaseTimestampStringToDisplay } from '../utils/formattingUtils';

export default function AlignItemsList() {

    const [data, setData] = useState<OrderAudit[]>([]);


    useEffect(() => {
        const getItems = async () => {
            const result = await orderService.fetchAllAudits()
            setData(result.data.reverse());
        }

        getItems();
    }, []);


    return (
        <div>
            {data.length > 0 && <Typography
                sx={{ display: 'inline' }}
                component="span"
                variant="h5"
                color="text.primary"
            >
                Order history
            </Typography>}
            <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                {
                    data.map((audit) =>
                        <div>
                            <ListItem alignItems="flex-start">
                                <ListItemAvatar>
                                    <Avatar alt={audit.dmlCreatedBy} src={`https://ui-avatars.com/api/?name=${audit.dmlCreatedBy}`} />
                                </ListItemAvatar>
                                <ListItemText
                                    primary={audit.id.dmlType + ': ' + audit.id.id}
                                    secondary={
                                        <React.Fragment>
                                            <Typography
                                                sx={{ display: 'inline' }}
                                                component="span"
                                                variant="body2"
                                                color="text.primary"
                                            >
                                                {audit.dmlCreatedBy} | {formatDatabaseTimestampStringToDisplay(audit.id!.dmlTimestamp!)}
                                            </Typography>



                                        </React.Fragment>
                                    }
                                />
                            </ListItem>
                            <Divider variant="inset" component="li" />
                        </div>
                    )
                }

            </List>
        </div>
    );
}