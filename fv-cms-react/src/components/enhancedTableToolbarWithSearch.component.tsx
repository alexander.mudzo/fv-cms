import { ClearOutlined } from "@mui/icons-material";
import { Toolbar, alpha, Typography, Tooltip, IconButton, FormControl, InputLabel, OutlinedInput, InputAdornment } from "@mui/material";
import React from "react";
import { useState } from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import FilterListIcon from '@mui/icons-material/FilterList';

interface EnhancedTableToolbarProps {
    numSelected: number;
    onDeletePressed: () => void;
    onSearchInputChanged: (val: string) => void;
    title?: string;
}

const EnhancedTableToolbarWithSearch = (props: EnhancedTableToolbarProps) => {
    const { numSelected, onSearchInputChanged } = props;

    const [showingSearch, setShowingSearch] = React.useState(false)

    const [searched, setSearched] = useState<string>("");

    const handleClearInput = () => {

        if (searched === "") {
            setShowingSearch(false)
        }
        handleInputChange("")
    }

    const handleInputChange = (val: string) => {
        setSearched(val)
        onSearchInputChanged(val)
    }

    return (
        <Toolbar
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
                ...(numSelected > 0 && {
                    bgcolor: (theme) =>
                        alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
                }),
            }}
        >
            {numSelected > 0 ? (
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    color="inherit"
                    variant="subtitle1"
                    component="div"
                >
                    {numSelected} selected
                </Typography>
            ) : (
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    variant="h6"
                    id="tableTitle"
                    component="div"
                >
                    {props.title ?? ''}
                </Typography>
            )}
            {numSelected > 0 ? (
                <Tooltip title="Delete">
                    <IconButton
                        onClick={(_) => props.onDeletePressed()}>
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            )
                : showingSearch ? (
                    <FormControl>
                        <InputLabel>Search</InputLabel>
                        <OutlinedInput
                            value={searched}
                            onChange={(e) => handleInputChange(e.target.value)}
                            id="outlined-search"
                            label="Search"
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="clear search input"
                                        onClick={handleClearInput}
                                        edge="end"
                                    >
                                        <ClearOutlined />
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>

                ) : (
                    <Tooltip title="Filter list">
                        <IconButton onClick={(_) => setShowingSearch(true)}>
                            <FilterListIcon />
                        </IconButton>
                    </Tooltip>
                )}
        </Toolbar>
    );
};

export default EnhancedTableToolbarWithSearch