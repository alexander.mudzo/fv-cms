import Category from "../types/category.type";
import ExportJobWarehouseItem from "../types/exportJobWarehouseItem";
import OrderWarehouseItem from "../types/orderWarehouseItem";
import WarehouseItem from "../types/warehouseItem.type";

export function formatCategoriesList(categories: Category[]): string {
    if (categories.length === 0) return ""
    else if (categories.length < 2) return categories[0]?.name ?? "";
    else return categories.map((c) => c.name ?? '').reduce((a, b) => a + ', ' + b)
}

export function formatWarehouseItemsList(items: WarehouseItem[]): string {
    if (items.length === 0) return ""
    else if (items.length < 2) return items[0]?.name ?? "";
    else return items.map((c) => c.name ?? '').reduce((a, b) => a + ', ' + b)
}

export function formatWarehouseItemsListWithCount(items: ExportJobWarehouseItem[] | OrderWarehouseItem[]): string {
    if (items.length === 0) return ""
    else if (items.length < 2) return `${(items[0]?.count ?? "")} ${(items[0]?.item?.name ?? "")}`;
    else return items.map((c) => `${(c?.count ?? "")} ${(c?.item?.name ?? "")}`).reduce((a, b) => a + ', ' + b)
}

export function formatDataseDateTimeStrigToDisplay(input?: string): string | undefined {
    if (input)
        return new Date(Date.parse(input)).toLocaleString('sk-SK')
}

export function formatDatabaseTimestampStringToDisplay(input?: string | number): string | undefined {
    if (input) {
        const parsedVal = typeof input === 'string' ? parseInt(input) : input;
        return new Date(parsedVal).toLocaleString('sk-SK')
    }

}