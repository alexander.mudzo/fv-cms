import axios from "axios";
import Supplier from "../../types/supplier";
import authHeader from "../auth-header";
import { BASE_URL } from "../helper";

const SUPPLIER_API_URL = `${BASE_URL}/suppliers/`;


class SupplierService {

    fetchAll() {
        return axios.get(
            SUPPLIER_API_URL, {headers: authHeader()}  
        );
    }


    fetchById(id: String) {
        return axios.get(
            SUPPLIER_API_URL + id, {headers: authHeader()} 
        );
    }

    createNewItem(item: Supplier) {
        return axios.post(SUPPLIER_API_URL,
             item, {headers: authHeader()} 
            )
    }

    updateItem(item: Supplier, id: String) {
        return axios.put(SUPPLIER_API_URL + id,
             item, {headers: authHeader()}  
            )
    }
    
    deleteItem(id: String, ) {
        return axios.delete(SUPPLIER_API_URL + id, {headers: authHeader()});
    }
 
    deleteItems(ids:readonly String[], ) {

        const params = '?' + ids.map((id) => `ids=${id}`).reduce((a,b) => a + '&' + b);
        return axios.delete(SUPPLIER_API_URL + params, {headers: authHeader()});
    }

}

export default new SupplierService()