
import axios from "axios";
import Category from "../../types/category.type";
import authHeader from "../auth-header";
import { BASE_URL } from "../helper";

const CATEGORIES_API_URL = `${BASE_URL}/categories/`;

class CategoriesService {

    fetchAll() {
        return axios.get(
            CATEGORIES_API_URL, {headers: authHeader()}
        );
    }

    fetchById(id: number) {
        return axios.get(
            CATEGORIES_API_URL + id, {headers: authHeader()}
        );
    }

    createNewItem(item: Category) {
        return axios.post(CATEGORIES_API_URL,
             item, {headers: authHeader()}
            )
    }

    updateItem(item: Category, id: number) {
        return axios.put(CATEGORIES_API_URL + id,
             item, {headers: authHeader()}
            )
    }
    
    deleteItem(id: number, ) {
        return axios.delete(CATEGORIES_API_URL + id,);
    }
 
}

export default new CategoriesService();