import axios from "axios";
import { BASE_URL } from "../helper";

const API_URL = `${BASE_URL}/auth/`;

class AuthService {
  login(username: string, password: string) {
    return axios
      .post(API_URL + "signin", {
        username,
        password
      })
      .then(response => {
        if (response.data) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(userName: string, email: string, password: string, name: string, lastName: string) {
    return axios.post(API_URL + "signup", {
      userName,
      email,
      password,
      name,
      lastName
    });
  }

  getCurrentUser() {
    const userStr = localStorage.getItem("user");
    if (userStr) return JSON.parse(userStr);

    return null;
  }
}

export default new AuthService();
