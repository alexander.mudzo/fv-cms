import axios from "axios";
import OrderStatus from "../../types/enums/orderStatus";
import OrderItem from "../../types/order.type";
import authHeader from "../auth-header";
import { BASE_URL } from "../helper";

const ORDER_ITEMS_API_URL = `${BASE_URL}/orders/`;

const AUDITS = 'audits'

class OrderItemService {

    fetchAll() {
        return axios.get(
            ORDER_ITEMS_API_URL, { headers: authHeader() }
        );
    }

    fetchById(id: number) {
        return axios.get(
            ORDER_ITEMS_API_URL + id, { headers: authHeader() }
        );
    }

    createNewItem(item: OrderItem) {
        return axios.post(ORDER_ITEMS_API_URL,
            item, { headers: authHeader() }
        )
    }

    updateItem(item: OrderItem, id: number) {
        return axios.put(ORDER_ITEMS_API_URL + id,
            item, { headers: authHeader() }
        )
    }   
    updateState(stat: OrderStatus, id: number) {
        return axios.patch(ORDER_ITEMS_API_URL + id,
            { status: stat }, { headers: authHeader() }
        )
    }

    deleteItem(id: number,) {
        return axios.delete(ORDER_ITEMS_API_URL + id, { headers: authHeader() });
    }

    deleteItems(ids:readonly number[],) {

        const params = '?' + ids.map((id) => `ids=${id}`).reduce((a, b) => a + '&' + b);
        return axios.delete(ORDER_ITEMS_API_URL + params, { headers: authHeader() });
    }

    fetchAllAudits() {
        return axios.get(
            ORDER_ITEMS_API_URL + AUDITS, { headers: authHeader() }
        );
    }

    fetchAuditById(id: number) {
        return axios.get(
            ORDER_ITEMS_API_URL + id + '/' + AUDITS, { headers: authHeader() }
        );
    }

}

export default new OrderItemService();