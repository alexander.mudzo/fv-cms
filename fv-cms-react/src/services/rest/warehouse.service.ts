import axios from "axios";
import WarehouseItem from "../../types/warehouseItem.type";
import authHeader from "../auth-header";
import { BASE_URL } from "../helper";

const WAREHOUSE_ITEMS_API_URL = `${BASE_URL}/warehouseitems/`;



class WarehouseService {

    fetchAll() {
        return axios.get(
            WAREHOUSE_ITEMS_API_URL, {headers: authHeader()}  
        );
    }


    fetchById(id: string) {
        return axios.get(
            WAREHOUSE_ITEMS_API_URL + id, {headers: authHeader()} 
        );
    }

    createNewItem(item: WarehouseItem) {
        return axios.post(WAREHOUSE_ITEMS_API_URL,
             item, {headers: authHeader()} 
            )
    }

    updateItem(item: WarehouseItem, id: string) {
        return axios.put(WAREHOUSE_ITEMS_API_URL + id,
             item, {headers: authHeader()}  
            )
    }
    
    deleteItem(id: string, ) {
        return axios.delete(WAREHOUSE_ITEMS_API_URL + id, {headers: authHeader()});
    }
 
    deleteItems(ids: readonly string[], ) {

        const params = '?' + ids.map((id) => `ids=${id}`).reduce((a,b) => a + '&' + b);
        return axios.delete(WAREHOUSE_ITEMS_API_URL + params, {headers: authHeader()});
    }

}

export default new WarehouseService();
