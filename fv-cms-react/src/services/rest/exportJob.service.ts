import axios from "axios";
import ExportJobStatus from "../../types/enums/exportJobStatus";
import ExportJob from "../../types/exportJob";
import authHeader from "../auth-header";
import { BASE_URL } from "../helper";


const EXPORT_JOBS_API_URL = `${BASE_URL}/exportjobs/`;


class ExportJobService {
    fetchAll() {
        return axios.get(
            EXPORT_JOBS_API_URL, {headers: authHeader()}  
        );
    }


    fetchById(id: number) {
        return axios.get(
            EXPORT_JOBS_API_URL + id, {headers: authHeader()} 
        );
    }

    createNewItem(item: ExportJob) {
        return axios.post(EXPORT_JOBS_API_URL,
             item, {headers: authHeader()} 
            )
    }

    updateItem(item: ExportJob, id: number) {
        return axios.put(EXPORT_JOBS_API_URL + id,
             item, {headers: authHeader()}  
            )
    }
    
    updateState(stat: ExportJobStatus, id: number) {
        return axios.patch(EXPORT_JOBS_API_URL + id,
            { status: stat }, { headers: authHeader() }
        )
    }

    deleteItem(id: number, ) {
        return axios.delete(EXPORT_JOBS_API_URL + id, {headers: authHeader()});
    }
 
    deleteItems(ids:readonly number[], ) {

        const params = '?' + ids.map((id) => `ids=${id}`).reduce((a,b) => a + '&' + b);
        return axios.delete(EXPORT_JOBS_API_URL + params, {headers: authHeader()});
    }
}

export default new ExportJobService()