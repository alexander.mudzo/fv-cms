import axios from "axios";
import eventBus from "../common/EventBus";

export const BASE_URL = "http://127.0.0.1:8080/api";


axios.interceptors.response.use((res) => {
    return res;
},
    (error) => {
        if (401 === error.response.status) {
            eventBus.dispatch("logout")
        } else {
            return Promise.reject(error);
        }
    }
)