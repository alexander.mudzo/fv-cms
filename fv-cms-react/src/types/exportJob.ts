import ExportJobStatus from "./enums/exportJobStatus"
import ExportJobWarehouseItem from "./exportJobWarehouseItem"

export default interface ExportJob {

    id?: number
    name?: string
    description?: string

    createdAt?: string
    updatedAt?: string
    deadline?: string

    status?: ExportJobStatus

    items: ExportJobWarehouseItem[]


}