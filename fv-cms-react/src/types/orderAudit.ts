import OrderAuditData from "./orderAuditData"
import OrderAuditKey from "./orderAuditKey"

export default interface OrderAudit {
    id: OrderAuditKey
    oldRowData?: OrderAuditData
    newRowData?: OrderAuditData
    dmlCreatedBy: string
}