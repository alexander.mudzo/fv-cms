export default interface OrderAuditData {
    created_at?: string,
    updated_at?: string,
    deleted?: boolean,
    order_id?: number,
    status?: number,
    supplier_id?: string
}