import WarehouseItem from "./warehouseItem.type";

export default interface ExportJobWarehouseItem {
    item? : WarehouseItem
    count?: number
    missingCount?: number
}