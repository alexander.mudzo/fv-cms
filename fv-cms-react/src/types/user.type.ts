export default interface IUser {
  id?: any | null,
  userName?: string | null,
  email?: string,
  roles?: Array<string>
  jwt?: string
}