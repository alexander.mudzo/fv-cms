import WarehouseItem from "./warehouseItem.type"

export default interface OrderWarehouseItem {
    
    count?: number
    item?: WarehouseItem
    
}