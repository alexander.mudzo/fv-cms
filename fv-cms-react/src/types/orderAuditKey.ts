export default interface OrderAuditKey {
    dmlTimestamp: number,
    id: number,
    dmlType: string
}