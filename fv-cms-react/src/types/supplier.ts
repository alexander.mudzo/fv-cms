export default interface Supplier {
    id?: string

    name?: string 

    createdAt?: string 

    updatedAt?: string
}