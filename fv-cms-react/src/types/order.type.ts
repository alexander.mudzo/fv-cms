import OrderStatus from "./enums/orderStatus"
import OrderWarehouseItem from "./orderWarehouseItem"
import Supplier from "./supplier"

export default interface OrderItem {
    id?: number

    createdAt?: string

    updatedAt?: string

    status?: OrderStatus

    items: OrderWarehouseItem[]

    supplier?: Supplier
}