import Category from "./category.type"

export default interface WarehouseItem {
    id?: string
    name?: string
    categories?: Category[]
    count?: number
    createdAt?: string
    updatedAt?: string
}