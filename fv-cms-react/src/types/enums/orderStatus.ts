enum OrderStatus {
    CREATED = "CREATED",
    PENDING = "PENDING",
    ACCEPTED = "ACCEPTED",
    IN_PROGRESS = "IN_PROGRESS",
    REJECTED = "REJECTED",
    DONE = "DONE",
}

export default OrderStatus