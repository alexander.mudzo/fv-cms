import { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AuthService from "./services/rest/auth.service";
import IUser from './types/user.type';

import Login from "./components/login.component";
import Register from "./components/register.component";
import Profile from "./components/profile.component";
import Home from './components/home.component'
import EventBus from "./common/EventBus";
import WarehouseItemsEnhancedTable from "./views/warehouseItemsTable";
import OrdersEnhancedTable from "./views/ordersTable";
import ExportJobsEnhancedTable from "./views/exportJobsTable";
import SuppliersEnhancedTable from "./views/suppliersTable";

type Props = {};

type State = {

  currentUser: IUser | undefined
}

class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      currentUser: undefined,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
      });
    }

    EventBus.on("logout", this.logOut);
  }

  componentWillUnmount() {
    EventBus.remove("logout", this.logOut);
  }

  logOut() {
    AuthService.logout();
    this.setState({
      currentUser: undefined,
    });
  }

  render() {
    const { currentUser } = this.state;

    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={(currentUser != null ? "/home" : "/")} className="navbar-brand">
            FVCMS
          </Link>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/home"} className="nav-link">
                Home
              </Link>
            </li>
            {
              (currentUser) &&
              (<li className="nav-item">
                <Link to={"/warehouse-items"} className="nav-link">
                  Warehouse
                </Link>
              </li>)
            }
            {
              (currentUser) &&
              (<li className="nav-item">
                <Link to={"/orders"} className="nav-link">
                  Orders
                </Link>
              </li>)
            }
            {
              (currentUser) &&
              (<li className="nav-item">
                <Link to={"/export-jobs"} className="nav-link">
                  Export jobs
                </Link>
              </li>)
            }
            {
              (currentUser) &&
              (<li className="nav-item">
                <Link to={"/suppliers"} className="nav-link">
                  Suppliers
                </Link>
              </li>)
            }
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.userName}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  Log Out
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Sign Up
                </Link>
              </li>
            </div>
          )}
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/login"]} component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/home" component={Home} />
            <Route exact path="/warehouse-items" component={WarehouseItemsEnhancedTable} />
            <Route exact path="/orders" component={OrdersEnhancedTable} />
            <Route exact path="/export-jobs" component={ExportJobsEnhancedTable} />
            <Route exact path="/suppliers" component={SuppliersEnhancedTable} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;