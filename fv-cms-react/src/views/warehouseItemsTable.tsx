import { useState, MouseEvent, ChangeEvent, useEffect } from 'react'
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import { visuallyHidden } from '@mui/utils';
import AddEditItemModal from '../modals/addEditItemModal';
import Grid from '@mui/material/Grid';
import WarehouseItem from '../types/warehouseItem.type';
import warehouseService from '../services/rest/warehouse.service';
import { formatCategoriesList, formatDataseDateTimeStrigToDisplay } from '../utils/formattingUtils';
import { getComparator, Order, stableSort } from '../utils/sortUtils';
import React from 'react';
import categoriesService from '../services/rest/categories.service';
import Category from '../types/category.type';
import EnhancedTableToolbarWithSearch from '../components/enhancedTableToolbarWithSearch.component';


interface HeadCell {
  disablePadding: boolean;
  id: keyof WarehouseItem;
  label: string;
  numeric: boolean;
}

const headCells: readonly HeadCell[] = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Name',
  },
  {
    id: 'categories',
    numeric: false,
    disablePadding: false,
    label: 'Categories',
  },
  {
    id: 'count',
    numeric: true,
    disablePadding: false,
    label: 'Count',
  },
  {
    id: 'createdAt',
    numeric: false,
    disablePadding: false,
    label: 'Created at',
  },
  {
    id: 'updatedAt',
    numeric: true,
    disablePadding: false,
    label: 'Updated at',
  },
];

interface EnhancedTableProps {
  numSelected: number;
  onRequestSort: (event: MouseEvent<unknown>, property: keyof WarehouseItem) => void;
  onSelectAllClick: (event: ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
    props;
  const createSortHandler =
    (property: keyof WarehouseItem) => (event: MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all',
            }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell></TableCell>
      </TableRow>
    </TableHead>
  );
}


export default function WarehouseItemsEnhancedTable() {
  const [order, setOrder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<keyof WarehouseItem>('count');
  const [selected, setSelected] = useState<readonly string[]>([]);
  const [page, setPage] = useState(0);
  const [dense, setDense] = useState(false);
  const [fetching, setFetching] = useState(true);

  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [data, setData] = useState<readonly WarehouseItem[]>([]);

  const [rows, setRows] = useState<WarehouseItem[]>([]);

  const [searched, setSearched] = useState<string>("");

  const [fetchedCategories, setFetchedCategories] = React.useState<Category[]>([]);


  const requestSearch = () => {

    const filteredRows = searched === "" ? [...data] : data.filter((row) => {
      return row.name!.toLowerCase().startsWith(searched.toLowerCase());
    });
    setRows(filteredRows);

  }

  const refreshWarehouseItems = () => {
    const getItems = async () => {
      setFetching(true)
      const result = await warehouseService.fetchAll()
      setData(result.data)
      setRows(result.data)
      setFetching(false)
    }
    getItems();
  }

  const refreshCategories = () => {
    const getItems = async () => {
      setFetching(true)
      const result = await categoriesService.fetchAll()
      setFetchedCategories(result.data);
      setFetching(false)
    }

    getItems();
  }

  useEffect(refreshWarehouseItems, []);

  useEffect(refreshCategories, [])

  useEffect(requestSearch, [searched])

  const handleRequestSort = (
    event: MouseEvent<unknown>,
    property: keyof WarehouseItem,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.id!);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };


  const isSelected = (id: string) => selected.indexOf(id) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const handleOnDeleteSelectedPressed = () => {
    warehouseService.deleteItems(selected).then((_) => refreshWarehouseItems())
  }

  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <EnhancedTableToolbarWithSearch
          numSelected={selected.length}
          title={"Warehouse Items"}
          onSearchInputChanged={(val) => {
            setSearched(val)
          }}
          onDeletePressed={handleOnDeleteSelectedPressed}
        />
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {/* if you don't need to support IE11, you can replace the `stableSort` call with:
              rows.slice().sort(getComparator(order, orderBy)) */}
              {stableSort<WarehouseItem>(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.id!);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.id!)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.id}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          color="primary"
                          checked={isItemSelected}
                          inputProps={{
                            'aria-labelledby': labelId,
                          }}
                        />
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        {row.name}
                      </TableCell>
                      <TableCell align="left">{formatCategoriesList(row.categories ?? [])}</TableCell>
                      <TableCell align="right">{row.count}</TableCell>
                      <TableCell align="left">{formatDataseDateTimeStrigToDisplay(row.createdAt)}</TableCell>
                      <TableCell align="right">{formatDataseDateTimeStrigToDisplay(row.updatedAt)}</TableCell>

                      <TableCell align="right">
                        {!fetching && <AddEditItemModal
                          categories={fetchedCategories}
                          item={row}
                          onCofirmed={(a) => refreshWarehouseItems()}
                        ></AddEditItemModal>}
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: (dense ? 33 : 53) * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>

      <Grid container>
        <Grid item xs={4}>
          {!fetching && <AddEditItemModal
            onCofirmed={(a) => refreshWarehouseItems()}
            categories={fetchedCategories}
          ></AddEditItemModal>}
        </Grid>
      </Grid>
    </Box>
  );
}