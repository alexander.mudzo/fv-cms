import { useState, MouseEvent, ChangeEvent, useEffect } from 'react'
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import { visuallyHidden } from '@mui/utils';
import Grid from '@mui/material/Grid';
import { formatDataseDateTimeStrigToDisplay, formatWarehouseItemsListWithCount } from '../utils/formattingUtils';
import { getComparator, Order, stableSort } from '../utils/sortUtils';
import ExportJob from '../types/exportJob';
import ExportJobService from '../services/rest/exportJob.service';
import AddEditExportJobModal from '../modals/addEditExportJobModal';
import warehouseService from '../services/rest/warehouse.service';
import WarehouseItem from '../types/warehouseItem.type';
import EnhancedTableToolbarWithSearch from '../components/enhancedTableToolbarWithSearch.component';
import Fab from '@mui/material/Fab';
import { PlayArrow } from '@mui/icons-material';
import ExportJobStatus from '../types/enums/exportJobStatus';
import exportJobService from '../services/rest/exportJob.service';


interface HeadCell {
    disablePadding: boolean;
    id: keyof ExportJob;
    label: string;
    numeric: boolean;
}

const headCells: readonly HeadCell[] = [
    {
        id: 'id',
        numeric: true,
        disablePadding: false,
        label: 'ID',
    },
    {
        id: 'name',
        numeric: false,
        disablePadding: false,
        label: 'Name',
    },
    {
        id: 'status',
        numeric: false,
        disablePadding: false,
        label: 'Status',
    },
    {
        id: 'items',
        numeric: false,
        disablePadding: false,
        label: 'Items',
    },
    {
        id: 'createdAt',
        numeric: true,
        disablePadding: false,
        label: 'Created at',
    },
    {
        id: 'updatedAt',
        numeric: true,
        disablePadding: false,
        label: 'Updated at',
    }
];

interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: MouseEvent<unknown>, property: keyof ExportJob) => void;
    onSelectAllClick: (event: ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
        props;
    const createSortHandler =
        (property: keyof ExportJob) => (event: MouseEvent<unknown>) => {
            onRequestSort(event, property);
        };

    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        color="primary"
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{
                            'aria-label': 'select all',
                        }}
                    />
                </TableCell>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <Box component="span" sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
                <TableCell></TableCell>
                <TableCell></TableCell>
            </TableRow>
        </TableHead>
    );
}

export default function ExportJobsEnhancedTable() {
    const [order, setOrder] = useState<Order>('asc');
    const [orderBy, setOrderBy] = useState<keyof ExportJob>('id');
    const [selected, setSelected] = useState<readonly number[]>([]);
    const [page, setPage] = useState(0);
    const [dense, setDense] = useState(false);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [data, setData] = useState<ExportJob[]>([]);
    const [items, setItems] = useState<WarehouseItem[]>([]);

    const [rows, setRows] = useState<ExportJob[]>([]);
    const [searched, setSearched] = useState<string>("");

    const requestSearch = () => {

        const filteredRows = searched === "" ? [...data] : data.filter((row) => {
            return row.name!.toLowerCase().startsWith(searched.toLowerCase());
        });
        setRows(filteredRows);


    }
    useEffect(requestSearch, [searched])

    const refreshJobs = () => {
        const getItems = async () => {
            const result = await ExportJobService.fetchAll()
            setData(result.data);
            setRows(result.data)
        }

        getItems();
    }


    useEffect(refreshJobs, []);


    useEffect(() => {
        const getItems = async () => {
            const result = await warehouseService.fetchAll()
            setItems(result.data);
        }

        getItems();
    }, []);

    const handleRequestSort = (
        event: MouseEvent<unknown>,
        property: keyof ExportJob,
    ) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelecteds = (data).map((n) => n.id!);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event: MouseEvent<unknown>, id: number) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected: readonly number[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };


    const isSelected = (id: number) => selected.indexOf(id) !== -1;

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

    const handleOnDeleteSelectedPressed = () => {
        ExportJobService.deleteItems(selected).then((_) => refreshJobs())
    }

    const handlePlayClicked = (item: ExportJob) => {


        let nextStatus;


        if (item.status === ExportJobStatus.CREATED) {
            nextStatus = ExportJobStatus.IN_PROGRESS
        }
        else if (item.status === ExportJobStatus.IN_PROGRESS) {
            nextStatus = ExportJobStatus.READY
        }
        else if (item.status === ExportJobStatus.BLOCKED) {
            nextStatus = ExportJobStatus.READY
        }
        else if (item.status === ExportJobStatus.READY) {
            nextStatus = ExportJobStatus.DONE
        }

        if (nextStatus) {

            exportJobService.updateState(nextStatus, item.id!).then((_) => refreshJobs())
        }

    }

    return (
        <Box sx={{ width: '100%' }}>
            <Paper sx={{ width: '100%', mb: 2 }}>
                <EnhancedTableToolbarWithSearch
                    numSelected={selected.length}
                    title={"Export jobs"}
                    onSearchInputChanged={(val) => {
                        setSearched(val)
                    }}
                    onDeletePressed={handleOnDeleteSelectedPressed}
                />

                <TableContainer>
                    <Table
                        sx={{ minWidth: 750 }}
                        aria-labelledby="tableTitle"
                        size={dense ? 'small' : 'medium'}
                    >
                        <EnhancedTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={handleSelectAllClick}
                            onRequestSort={handleRequestSort}
                            rowCount={rows.length}
                        />
                        <TableBody>
                            {/* if you don't need to support IE11, you can replace the `stableSort` call with:
              rows.slice().sort(getComparator(order, orderBy)) */}
                            {stableSort<ExportJob>(rows, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.id!);
                                    const labelId = `enhanced-table-checkbox-${index}`;

                                    return (
                                        <TableRow
                                            hover
                                            onClick={(event) => handleClick(event, row.id!)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.id}
                                            selected={isItemSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox
                                                    color="primary"
                                                    checked={isItemSelected}
                                                    inputProps={{
                                                        'aria-labelledby': labelId,
                                                    }}
                                                />
                                            </TableCell>
                                            <TableCell
                                                align="center"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >
                                                {row.id}
                                            </TableCell>
                                            <TableCell
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >
                                                {row.name}
                                            </TableCell>
                                            <TableCell
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >
                                                {row.status}
                                            </TableCell>
                                            <TableCell align="right"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >
                                                {formatWarehouseItemsListWithCount(row.items)}
                                            </TableCell>
                                            <TableCell
                                                align="right"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >{formatDataseDateTimeStrigToDisplay(row.createdAt)}
                                            </TableCell>
                                            <TableCell align="right"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >{formatDataseDateTimeStrigToDisplay(row.updatedAt)}</TableCell>
                                            <TableCell align="right">
                                                <AddEditExportJobModal
                                                    job={row}
                                                    items={items}
                                                    onCofirmed={(a) => refreshJobs()}
                                                ></AddEditExportJobModal>
                                            </TableCell>

                                            <TableCell>
                                                {row.status !== ExportJobStatus.DONE && <Fab size="medium" color="primary" aria-label="start"
                                                    onClick={(e) => handlePlayClicked(row)}
                                                >
                                                    <PlayArrow />
                                                </Fab>}
                                            </TableCell>

                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow
                                    style={{
                                        height: (dense ? 33 : 53) * emptyRows,
                                    }}
                                >
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>

            <Grid container>
                <Grid item xs={4}>
                    <AddEditExportJobModal
                        items={items}
                        onCofirmed={(a) => refreshJobs()}
                    ></AddEditExportJobModal>
                </Grid>
            </Grid>
        </Box>
    );
}