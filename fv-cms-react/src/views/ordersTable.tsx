import { useState, MouseEvent, ChangeEvent, useEffect } from 'react'
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import DeleteIcon from '@mui/icons-material/Delete';
import { visuallyHidden } from '@mui/utils';
import Grid from '@mui/material/Grid';
import OrderItemService from '../services/rest/order.service';
import { formatDataseDateTimeStrigToDisplay, formatWarehouseItemsListWithCount } from '../utils/formattingUtils';
import { getComparator, Order, stableSort } from '../utils/sortUtils';
import OrderItem from '../types/order.type';
import AddEditOrderModal from '../modals/addEditOrderModal';
import supplierService from '../services/rest/supplier.service';
import warehouseService from '../services/rest/warehouse.service';
import Supplier from '../types/supplier';
import WarehouseItem from '../types/warehouseItem.type';
import Fab from '@mui/material/Fab';
import { PlayArrow } from '@mui/icons-material';
import OrderStatus from '../types/enums/orderStatus';
import orderService from '../services/rest/order.service';


interface HeadCell {
    disablePadding: boolean;
    id: keyof OrderItem;
    label: string;
    numeric: boolean;
}

const headCells: readonly HeadCell[] = [
    {
        id: 'id',
        numeric: true,
        disablePadding: false,
        label: 'ID',
    },
    {
        id: 'status',
        numeric: false,
        disablePadding: false,
        label: 'Status',
    },
    {
        id: 'items',
        numeric: false,
        disablePadding: false,
        label: 'Items',
    },
    {
        id: 'createdAt',
        numeric: true,
        disablePadding: false,
        label: 'Created at',
    },
    {
        id: 'updatedAt',
        numeric: true,
        disablePadding: false,
        label: 'Updated at',
    },
    {
        id: 'supplier',
        numeric: false,
        disablePadding: false,
        label: 'Supplier',
    },

];

interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: MouseEvent<unknown>, property: keyof OrderItem) => void;
    onSelectAllClick: (event: ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
        props;
    const createSortHandler =
        (property: keyof OrderItem) => (event: MouseEvent<unknown>) => {
            onRequestSort(event, property);
        };

    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        color="primary"
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{
                            'aria-label': 'select all desserts',
                        }}
                    />
                </TableCell>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <Box component="span" sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
                <TableCell></TableCell>
                <TableCell></TableCell>
            </TableRow>
        </TableHead>
    );
}

interface EnhancedTableToolbarProps {
    numSelected: number;
    onDeletePressed: () => void;

}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const { numSelected } = props;

    return (
        <Toolbar
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
                ...(numSelected > 0 && {
                    bgcolor: (theme) =>
                        alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
                }),
            }}
        >
            {numSelected > 0 ? (
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    color="inherit"
                    variant="subtitle1"
                    component="div"
                >
                    {numSelected} selected
                </Typography>
            ) : (
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    variant="h6"
                    id="tableTitle"
                    component="div"
                >
                    Orders
                </Typography>
            )}
            {numSelected > 0 && (
                <Tooltip title="Delete">
                    <IconButton
                        onClick={(_) => props.onDeletePressed()}>
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            )}
        </Toolbar>
    );
};

export default function OrdersEnhancedTable() {
    const [order, setOrder] = useState<Order>('asc');
    const [orderBy, setOrderBy] = useState<keyof OrderItem>('id');
    const [selected, setSelected] = useState<readonly number[]>([]);
    const [page, setPage] = useState(0);
    const [dense, setDense] = useState(false);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [data, setData] = useState<OrderItem[]>([]);

    const [items, setItems] = useState<WarehouseItem[]>([]);

    const [suppliers, setSuppliers] = useState<Supplier[]>([]);



    useEffect(() => {
        const getItems = async () => {
            const result = await warehouseService.fetchAll()
            setItems(result.data);

            const supps = await supplierService.fetchAll()
            setSuppliers(supps.data);
        }

        getItems();
    }, []);

    useEffect(() => {
        const getItems = async () => {
            const result = await OrderItemService.fetchAll()
            setData(result.data);
        }

        getItems();
    }, []);




    const handleRequestSort = (
        event: MouseEvent<unknown>,
        property: keyof OrderItem,
    ) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelecteds = (data).map((n) => n.id!);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event: MouseEvent<unknown>, id: number) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected: readonly number[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };


    const isSelected = (id: number) => selected.indexOf(id) !== -1;

    const refreshOrders = () => {
        const getItems = async () => {
            const result = await OrderItemService.fetchAll()
            setData(result.data);
        }
        getItems();
    }

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

    const handlePlayClicked = (item: OrderItem) => {

        let nextStatus;

        if (item.status === OrderStatus.CREATED) {
            nextStatus = OrderStatus.PENDING
        }
        else if (item.status === OrderStatus.PENDING) {
            nextStatus = OrderStatus.ACCEPTED
        }
        else if (item.status === OrderStatus.ACCEPTED) {
            nextStatus = OrderStatus.IN_PROGRESS
        }
        else if (item.status === OrderStatus.IN_PROGRESS) {
            nextStatus = OrderStatus.DONE
        }

        if (nextStatus) {

            orderService.updateState(nextStatus, item.id!).then((_) => refreshOrders())
        }

    }


    return (
        <Box sx={{ width: '100%' }}>
            <Paper sx={{ width: '100%', mb: 2 }}>
                <EnhancedTableToolbar
                    numSelected={selected.length}
                    onDeletePressed={
                        () => OrderItemService.deleteItems(selected).then((_) => refreshOrders())
                    }
                />
                <TableContainer>
                    <Table
                        sx={{ minWidth: 750 }}
                        aria-labelledby="tableTitle"
                        size={dense ? 'small' : 'medium'}
                    >
                        <EnhancedTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={handleSelectAllClick}
                            onRequestSort={handleRequestSort}
                            rowCount={data.length}
                        />
                        <TableBody>
                            {/* if you don't need to support IE11, you can replace the `stableSort` call with:
              rows.slice().sort(getComparator(order, orderBy)) */}
                            {stableSort<OrderItem>(data, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.id!);
                                    const labelId = `enhanced-table-checkbox-${index}`;

                                    return (
                                        <TableRow
                                            hover
                                            onClick={(event) => handleClick(event, row.id!)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.id}
                                            selected={isItemSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox
                                                    color="primary"
                                                    checked={isItemSelected}
                                                    inputProps={{
                                                        'aria-labelledby': labelId,
                                                    }}
                                                />
                                            </TableCell>
                                            <TableCell
                                                align="center"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >
                                                {row.id}
                                            </TableCell>
                                            <TableCell
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >
                                                {row.status}
                                            </TableCell>
                                            <TableCell align="right"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >{formatWarehouseItemsListWithCount(row.items)}</TableCell>
                                            <TableCell align="right"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >{formatDataseDateTimeStrigToDisplay(row.createdAt)}</TableCell>
                                            <TableCell align="right"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >{formatDataseDateTimeStrigToDisplay(row.updatedAt)}</TableCell>
                                            <TableCell align="right"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >{row.supplier?.name}</TableCell>
                                            <TableCell align="right">
                                                <AddEditOrderModal
                                                    order={row}
                                                    items={items}
                                                    suppliers={suppliers}
                                                    onCofirmed={(a) => refreshOrders()}
                                                ></AddEditOrderModal>
                                            </TableCell>

                                            <TableCell>
                                                {row.status !== OrderStatus.DONE && <Fab size="medium" color="primary" aria-label="start"
                                                    onClick={(e) => handlePlayClicked(row)}
                                                >
                                                    <PlayArrow />
                                                </Fab>}
                                            </TableCell>

                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow
                                    style={{
                                        height: (dense ? 33 : 53) * emptyRows,
                                    }}
                                >
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>

            <Grid container>
                <Grid item xs={4}>
                    <AddEditOrderModal
                        items={items}
                        suppliers={suppliers}
                        onCofirmed={(a) => refreshOrders()}
                    ></AddEditOrderModal>
                </Grid>
            </Grid>
        </Box>
    );
}