package com.xps.fvcms;

import com.xps.fvcms.model.Role;
import com.xps.fvcms.model.User;
import com.xps.fvcms.repository.RoleRepository;
import com.xps.fvcms.repository.UserRepository;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = {UserRepositoryIntegrationTest.Initializer.class})
@SpringBootTest
public class UserRepositoryIntegrationTest {
    // A dummy class whose sole purpose is to instantiate the `SELF`
    // type parameter.

    @ClassRule
    public static MyPostgreSQLContainer MY_POSTGRE_SQL_CONTAINER = MyPostgreSQLContainer.getInstance();

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Test
    public void testSaveNewUser() {
        userRepository.deleteAll();
        roleRepository.deleteAll();

        Role role = roleRepository.save(Role.builder().role("ROLE_ADMIN").build());
        User user = userRepository.save(User.builder()
                .name("Test")
                .lastName("Admin")
                .email("test@test.com")
                .userName("test.admin")
                .active(true)
                .roles(new HashSet<>(List.of(role)))
                .password(bCryptPasswordEncoder.encode("123456789"))
                .build());

        assertThat(user)
                .matches(u -> u.getId() != null && u.getName().equals("Test") && u.getUserName().equals("test.admin"));
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + MY_POSTGRE_SQL_CONTAINER.getJdbcUrl(),
                    "spring.datasource.username=" + MY_POSTGRE_SQL_CONTAINER.getUsername(),
                    "spring.datasource.password=" + MY_POSTGRE_SQL_CONTAINER.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
