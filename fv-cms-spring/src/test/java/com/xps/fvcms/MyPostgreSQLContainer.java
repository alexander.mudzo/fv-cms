package com.xps.fvcms;

import org.testcontainers.containers.PostgreSQLContainer;

public final class MyPostgreSQLContainer extends PostgreSQLContainer<MyPostgreSQLContainer> {

    private static MyPostgreSQLContainer container;


    public MyPostgreSQLContainer() {
        super("postgres:latest");
    }
    public static MyPostgreSQLContainer getInstance() {
        if (container == null) {
            container = new MyPostgreSQLContainer();
        }
        return container;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}
