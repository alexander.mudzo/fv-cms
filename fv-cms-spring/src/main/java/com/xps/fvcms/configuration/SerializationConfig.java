package com.xps.fvcms.configuration;

import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.UUID;

@Configuration
public class SerializationConfig {

    private static final String dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String dateFormat = "yyyy-MM-dd";


    @Bean
    public ModelMapper buildModelMapper() {

        ModelMapper mapper = new ModelMapper();

        SimpleDateFormat df =  new SimpleDateFormat(dateTimeFormat);

        Converter<Date, String> formatDate = ctx -> ctx.getSource() != null
                ? df.format(ctx.getSource())
                : null;

        Converter<String, Date> parseDate = ctx -> {
            try {
                return ctx.getSource() != null
                        ? df.parse(ctx.getSource())
                        : null;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        };

        mapper.addConverter(formatDate);
        mapper.addConverter(parseDate);
        return mapper;
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> {
            builder.simpleDateFormat(dateTimeFormat);
            builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern(dateFormat)));
            builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(dateTimeFormat)));
            builder.serializers(new DateSerializer().withFormat(true, new SimpleDateFormat(dateTimeFormat)));
        };
    }

}
