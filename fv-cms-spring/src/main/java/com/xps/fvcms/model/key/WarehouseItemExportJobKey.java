package com.xps.fvcms.model.key;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseItemExportJobKey implements Serializable {
    @Column(name = "job_id")
    private Integer jobId;

    @Column(name = "item_id")
    private UUID itemId;

}
