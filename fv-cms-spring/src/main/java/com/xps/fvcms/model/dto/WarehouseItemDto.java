package com.xps.fvcms.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Set;
import java.util.UUID;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseItemDto {

    private UUID id;

    private String name;

    private Integer count;

    private Boolean deleted;

    private String createdAt;

    private String updatedAt;

    private Float price;

    private String description;

    private Set<CategoryDto> categories;

}
