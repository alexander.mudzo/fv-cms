package com.xps.fvcms.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.xps.fvcms.model.JobStatus;
import lombok.*;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ExportJobDto {

    private Integer id;

    private String name;

    private String description;

    private String createdAt;

    private String updatedAt;

    private String deadline;

    private JobStatus status;

    private List<WarehouseItemExportJobDto> items;

}
