package com.xps.fvcms.model.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseItemOrderDto {

    private WarehouseItemDto item;

    private Integer count;

}
