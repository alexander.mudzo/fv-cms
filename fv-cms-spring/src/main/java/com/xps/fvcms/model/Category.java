package com.xps.fvcms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "category_id", nullable = false)
    private Integer id;

    @Column(unique = true)
    @NotNull
    @NotEmpty
    @NotBlank
    private String name;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private Boolean deleted;
    @ToString.Exclude
    @ManyToMany(mappedBy = "categories",cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<WarehouseItem> items = new HashSet<>();

    public void addItem(WarehouseItem item){

        //assumes equals and hashcode implemented: avoid circular calls
        if(! items.contains(item)){
            items.add(item);

            //add method to Product : sets 'other side' of association
            item.addCategory(this);
        }
    }

    public void removeItem(WarehouseItem item){

        //assumes equals and hashcode implemented: avoid circular calls
        if(items.contains(item)){
            items.remove(item);

            //add method to Product : sets 'other side' of association
            item.removeCategory(this);
        }
    }
}
