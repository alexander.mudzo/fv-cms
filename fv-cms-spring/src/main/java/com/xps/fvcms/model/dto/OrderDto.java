package com.xps.fvcms.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.xps.fvcms.model.OrderStatus;
import lombok.*;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

    private Integer id;

    private String createdAt;

    private String updatedAt;

    private OrderStatus status;

    private List<WarehouseItemOrderDto> items;

    private SupplierDto supplier;

}
