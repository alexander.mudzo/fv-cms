package com.xps.fvcms.model.key;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseItemOrderKey implements Serializable {

    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "item_id")
    private UUID itemId;

}
