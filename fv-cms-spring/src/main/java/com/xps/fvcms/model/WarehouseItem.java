package com.xps.fvcms.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDateTime;
import java.util.*;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "warehouse_items")
public class WarehouseItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "item_id", nullable = false)
    private UUID id;

    @NotNull
    @NotBlank
    @NotEmpty(message = "*Please provide item name")
    @Column(name = "name")
    private String name;

    @PositiveOrZero(message = "Item count can be positive number or zero!")
    @Column(nullable = false)
    private Integer count;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private Boolean deleted;

    private Float price;

    private String description;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "item_category",
            joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private Set<Category> categories  = new HashSet<>();

    @OneToMany(mappedBy = "item")
    @ToString.Exclude
    private Set<WarehouseItemOrder> orders = new HashSet<>();

    @OneToMany(mappedBy = "item")
    @ToString.Exclude
    Set<WarehouseItemExportJob> jobs = new HashSet<>();

    public void addCategory(Category category){

        //avoid circular calls : assumes equals and hashcode implemented
        if(! categories.contains(category)){
            categories.add(category);

            //add method to Category : sets 'other side' of association
            category.addItem(this);
        }
    }

    public void removeCategory(Category category){

        //avoid circular calls: assumes equals and hashcode implemented:
        if(categories.contains(category)){
            categories.remove(category);

            //add method to Category: set 'other side' of association:
            category.removeItem(this);
        }
    }

    public void removeAllCategories() {
        List.copyOf(categories).forEach(this::removeCategory);
    }


    public void addOrder(WarehouseItemOrder order){

        //avoid circular calls : assumes equals and hashcode implemented
        if(! orders.contains(order)){
            orders.add(order);

            //add method to Category : sets 'other side' of association
            order.setItem(this);
        }
    }

    public void removeOrder(WarehouseItemOrder order){

        //avoid circular calls: assumes equals and hashcode implemented:
        if(orders.contains(order)){
            orders.remove(order);

            //add method to Category: set 'other side' of association:
            order.setItem(null);
        }
    }

    public void addJob(WarehouseItemExportJob job){

        //avoid circular calls : assumes equals and hashcode implemented
        if(! jobs.contains(job)){
            jobs.add(job);

            //add method to WarehouseItemExportJob : sets 'other side' of association
            job.setItem(this);
        }
    }

    public void removeJob(WarehouseItemExportJob job){

        //avoid circular calls: assumes equals and hashcode implemented:
        if(jobs.contains(job)){
            jobs.remove(job);

            //add method to Category: set 'other side' of association:
            job.setItem(null);
        }
    }


}
