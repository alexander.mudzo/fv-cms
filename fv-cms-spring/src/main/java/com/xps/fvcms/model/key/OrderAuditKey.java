package com.xps.fvcms.model.key;

import com.xps.fvcms.model.DmlOption;
import com.xps.fvcms.model.PostgreSQLEnumType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
public class OrderAuditKey implements Serializable {
    @Column(name = "order_id", nullable = false)
    private Integer id;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "dml_type")
    @Type(type = "pgsql_enum")
    private DmlOption dmlType;

    private Timestamp dmlTimestamp;
}
