package com.xps.fvcms.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "suppliers")
public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "supplier_id", nullable = false)
    private UUID id;

    @NotNull
    private  String name;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private Boolean deleted;

    @ToString.Exclude
    @OneToMany(mappedBy = "supplier")
    private Set<Order> orders = new HashSet<>();


    public void addOrder(Order order) {

        if(!orders.contains(order)) {
            orders.add(order);
            order.setSupplier(this);
        }

    }

    public void removeOrder(Order order) {
        if(orders.contains(order)) {
            orders.remove(order);
            order.removeSupplier();
        }
    }


}
