package com.xps.fvcms.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonType;
import com.xps.fvcms.model.key.OrderAuditKey;
import lombok.*;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders_audit_log")
@TypeDef(
        typeClass = JsonType.class,
        defaultForType = JsonNode.class
)
public class OrderAudit {

    @EmbeddedId
    private OrderAuditKey id = new OrderAuditKey();

    @Column(columnDefinition = "jsonb")
    private JsonNode oldRowData;

    @Column(columnDefinition = "jsonb")
    private JsonNode newRowData;

    private String dmlCreatedBy;

}
