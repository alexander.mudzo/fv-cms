package com.xps.fvcms.model;

import com.xps.fvcms.model.key.WarehouseItemOrderKey;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "order_items")
public class WarehouseItemOrder {

    @EmbeddedId
    WarehouseItemOrderKey id = new WarehouseItemOrderKey();

    @ManyToOne
    @MapsId("itemId")
    @JoinColumn(name = "item_id")
    WarehouseItem item;

    @ManyToOne
    @MapsId("orderId")
    @JoinColumn(name = "order_id")
    Order order;

    @PositiveOrZero
    @NotNull
    Integer count;
}
