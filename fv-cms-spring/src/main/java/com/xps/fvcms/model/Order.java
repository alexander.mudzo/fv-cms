package com.xps.fvcms.model;

import lombok.*;
import org.apache.tools.ant.taskdefs.War;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;

    @OneToMany(mappedBy = "order")
    @ToString.Exclude
    private Set<WarehouseItemOrder> items = new HashSet<>();

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private Boolean deleted;

    private OrderStatus status;


    public void setSupplier(Supplier supplier) {
        removeSupplier();
        supplier.addOrder(this);
        this.supplier = supplier;
    }

    public void removeSupplier() {
        if(this.supplier!=null) {
            supplier.removeOrder(this);
            this.supplier = null;
        }
    }

    public void addItem(WarehouseItemOrder item){

        if(!items.contains(item)) {
            items.add(item);
            item.setOrder(this);

        }

    }

    public void removeItem(WarehouseItemOrder item){
        if(items.contains(item)) {
            items.remove(item);
            item.setOrder(null);
        }
    }

}
