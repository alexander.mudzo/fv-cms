package com.xps.fvcms.model;

import com.xps.fvcms.model.key.WarehouseItemExportJobKey;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Entity@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "export_job_items")
public class WarehouseItemExportJob {

    @EmbeddedId
    WarehouseItemExportJobKey id = new WarehouseItemExportJobKey();


    @ManyToOne
    @MapsId("itemId")
    @JoinColumn(name = "item_id")
    WarehouseItem item;

    @ManyToOne
    @MapsId("jobId")
    @JoinColumn(name = "job_id")
    ExportJob job;

    @PositiveOrZero
    @NotNull
    Integer count;

    @PositiveOrZero
    @NotNull
    Integer missingCount;

    Boolean automaticOrderCreated;

}
