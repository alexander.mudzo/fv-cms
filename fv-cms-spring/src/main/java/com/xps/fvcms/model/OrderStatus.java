package com.xps.fvcms.model;

public enum OrderStatus {
    CREATED, PENDING, ACCEPTED, IN_PROGRESS, REJECTED, DONE,
}
