package com.xps.fvcms.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "export_jobs")
public class ExportJob {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "job_id", nullable = false)
    private Integer id;

    private String name;

    private String description;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deadline;

    private Boolean deleted;

    private JobStatus status;


    @OneToMany(mappedBy = "job")
    @ToString.Exclude
    private Set<WarehouseItemExportJob> items = new HashSet<>();


    public void addItem(WarehouseItemExportJob item){

        //avoid circular calls : assumes equals and hashcode implemented
        if(! items.contains(item)){
            items.add(item);

            //add method to WarehouseItemExportJob : sets 'other side' of association
            item.setJob(this);
        }
    }

    public void removeItem(WarehouseItemExportJob item){

        //avoid circular calls: assumes equals and hashcode implemented:
        if(items.contains(item)){
            items.remove(item);

            //add method to Category: set 'other side' of association:
            item.setJob(null);
        }
    }

}
