package com.xps.fvcms.service;


import com.xps.fvcms.model.WarehouseItem;
import com.xps.fvcms.repository.WarehouseItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class WarehouseItemService {

    private final WarehouseItemRepository warehouseItemRepository;


    @Autowired
    public WarehouseItemService(WarehouseItemRepository warehouseItemRepository) {
        this.warehouseItemRepository = warehouseItemRepository;
    }

    public List<WarehouseItem> findAllByName(String name) {
        return warehouseItemRepository.findAllByName(name);
    }

    public List<WarehouseItem> findAll() {
        return warehouseItemRepository.findAll();
    }

    public List<WarehouseItem> findByCategoryName(String name) {
        return warehouseItemRepository.findAllByCategories_Name(name);
    }

    public  List<WarehouseItem> findByCategoryId(Integer id) {
        return  warehouseItemRepository.findWarehouseItemsByCategoriesId(id);
    }

    public WarehouseItem findById(UUID id) {
        return warehouseItemRepository.findById(id).orElse(null);
    }

    public WarehouseItem addItem(WarehouseItem item) {
        item.setCreatedAt(LocalDateTime.now());
        return  warehouseItemRepository.create(item);
    }

    public WarehouseItem updateItem(UUID id, WarehouseItem item) {
        item.setUpdatedAt(LocalDateTime.now());
        return warehouseItemRepository.update(id, item);
    }


    public void deleteItemById(UUID id) {
        warehouseItemRepository.cascadeDeleteById(id);
    }


    public void deleteItemsByIds(List<UUID> ids) {
        warehouseItemRepository.deleteByIds(ids);
    }

}
