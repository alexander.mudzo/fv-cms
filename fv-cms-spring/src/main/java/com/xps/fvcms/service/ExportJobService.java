package com.xps.fvcms.service;

import com.xps.fvcms.model.ExportJob;
import com.xps.fvcms.model.JobStatus;
import com.xps.fvcms.repository.ExportJobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ExportJobService {

    private final ExportJobRepository exportJobRepository;

    @Autowired
    public ExportJobService(ExportJobRepository exportJobRepository) {
        this.exportJobRepository = exportJobRepository;
    }

    public List<ExportJob> findAll() {
        return exportJobRepository.findAll();
    }


    public ExportJob findById(Integer id) {
        return exportJobRepository.findById(id).orElse(null);
    }

    public ExportJob addJob(ExportJob job) {
        job.setCreatedAt(LocalDateTime.now());
        job.setStatus(JobStatus.CREATED);
        return  exportJobRepository.create(job);
    }

    public ExportJob updateJob(Integer id, ExportJob job) {
        job.setUpdatedAt(LocalDateTime.now());
        return exportJobRepository.update(id, job);
    }

    public void deleteJobById(Integer id) {
        exportJobRepository.cascadeDeleteById(id);
    }

    public void deleteJobsByIds(List<Integer> ids) {
        exportJobRepository.deleteByIds(ids);
    }

    public ExportJob updateState(Integer id, ExportJob job) {
        return exportJobRepository.updateState(id, job);
    }

}
