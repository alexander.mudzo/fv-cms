package com.xps.fvcms.service;

import com.xps.fvcms.model.Order;
import com.xps.fvcms.model.OrderAudit;
import com.xps.fvcms.model.OrderStatus;
import com.xps.fvcms.repository.OrderAuditRepository;
import com.xps.fvcms.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    private final OrderAuditRepository auditRepository;


    @Autowired
    public OrderService(OrderRepository orderRepository, OrderAuditRepository auditRepository) {
        this.orderRepository = orderRepository;
        this.auditRepository = auditRepository;
    }

    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public Order findById(Integer id) {
        return orderRepository.findById(id).orElse(null);
    }

    public Order addItem(Order item) {
        item.setCreatedAt(LocalDateTime.now());
        item.setStatus(OrderStatus.CREATED);
        return orderRepository.create(item);
    }

    public Order updateItem(Integer id, Order item) {
        item.setUpdatedAt(LocalDateTime.now());
        return orderRepository.update(id, item);
    }

    public void deleteItemById(Integer id) {
        orderRepository.cascadeDeleteById(id);
    }

    public List<OrderAudit> findAllAudits() {
        return auditRepository.findAll();
    }

    public List<OrderAudit> findAuditsByOrderId(Integer id) {
        return auditRepository.findAllById(Collections.singleton(id));
    }

    public Order updateState(Integer id, Order order) {
        return orderRepository.updateState(id, order);
    }

    public void deleteItemsByIds(List<Integer> ids) {
        orderRepository.deleteByIds(ids);
    }
}
