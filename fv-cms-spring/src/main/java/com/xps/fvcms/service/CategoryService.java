package com.xps.fvcms.service;

import com.xps.fvcms.model.Category;
import com.xps.fvcms.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category findByName(String name ) {
        return categoryRepository.findByName(name);
    }

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    public List<Category> findCategoriesByItemsId(UUID itemId) {
        return categoryRepository.findCategoriesByItemsId(itemId);
    }

    public Category findById(Integer id) {
        return categoryRepository.findById(id).orElse(null);
    }



    public Category addCategory(Category category) {
        category.setCreatedAt(LocalDateTime.now());
        return  categoryRepository.save(category);
    }

    public Category updateCategory(Integer id, Category category) {
        category.setUpdatedAt(LocalDateTime.now());
        return categoryRepository.update(id, category);
    }

    public void deleteCategoryById(Integer id) {
        categoryRepository.cascadeDeleteById(id);
    }

    public void deleteCategoriesByIds(List<Integer> ids) {
        categoryRepository.deleteByIds(ids);
    }
}
