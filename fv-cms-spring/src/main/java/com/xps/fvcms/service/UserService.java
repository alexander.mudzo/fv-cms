package com.xps.fvcms.service;

import com.xps.fvcms.exceptions.MyResourceNotFoundException;
import com.xps.fvcms.model.Role;
import com.xps.fvcms.model.User;
import com.xps.fvcms.repository.RoleRepository;
import com.xps.fvcms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> new MyResourceNotFoundException("user with email " + email + "not found!"));
    }

    public User findUserByUserName(String userName) {
        return userRepository.findByUserName(userName).orElseThrow(() -> new MyResourceNotFoundException("user with username " + userName + "not found!"));
    }

    public User saveUser(User user, String role) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(true);

        Set<Role> roles = new HashSet<>();

        Role userRole = roleRepository.findByRole(role)
                .orElseThrow(() -> new MyResourceNotFoundException("Error: Role" + role + " is not found."));
        roles.add(userRole);

        user.setRoles(roles);


        return userRepository.save(user);
    }

    public Boolean existsByUserName(String userName) {
        return userRepository.existsByUserName(userName);
    }

    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

}