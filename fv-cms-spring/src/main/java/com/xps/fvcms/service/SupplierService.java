package com.xps.fvcms.service;


import com.xps.fvcms.model.Supplier;
import com.xps.fvcms.model.WarehouseItem;
import com.xps.fvcms.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class SupplierService {

    private final SupplierRepository supplierRepository;


    @Autowired
    public SupplierService(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    public List<Supplier> findAll() {
        return supplierRepository.findAll();
    }

    public Supplier findById(UUID id) {
        return supplierRepository.findById(id).orElse(null);
    }

    public Supplier addItem(Supplier item) {
        item.setCreatedAt(LocalDateTime.now());
        return  supplierRepository.save(item);
    }

    public Supplier updateItem(UUID id, Supplier item) {
        item.setUpdatedAt(LocalDateTime.now());
        return supplierRepository.update(id, item);
    }


    public void deleteItemById(UUID id) {
        supplierRepository.cascadeDeleteById(id);
    }

    public void deleteItemsByIds(List<UUID> ids) {
        supplierRepository.deleteByIds(ids);
    }
}
