package com.xps.fvcms.auth.entities;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequest {

    private  String email;
    private  String userName;
    private  String password;
    private  String name;
    private  String lastName;

}
