package com.xps.fvcms.auth;

import com.xps.fvcms.model.User;
import com.xps.fvcms.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class AuthInitializer {

    private static final String ADMIN_NAME = "admin";
    private static final String ADMIN_EMAIL = "admin@adminemail.com"; //TODO replace with real value

    private static final Logger logger = LoggerFactory.getLogger(AuthEntryPointJwt.class);


    @Autowired
    UserService userService;

    @PostConstruct
    public void init() {
        if (userService.existsByUserName(ADMIN_NAME) || userService.existsByEmail(ADMIN_EMAIL)) {
            logger.debug("admin user already created");
            return;
        }

        final String password = String.valueOf(UUID.randomUUID());

        logger.warn("!!! New admin user will be created. Please note down following credentials. !!!");
        logger.warn("username: " + ADMIN_NAME);
        logger.warn("email: " + ADMIN_EMAIL);
        logger.warn("password: " + password);
        // Create new user's account
        User admin = new User(
                ADMIN_NAME,
                ADMIN_EMAIL,
                password,
                ADMIN_NAME,
                ADMIN_NAME
        );
        userService.saveUser(admin, "ROLE_ADMIN");
    }
}
