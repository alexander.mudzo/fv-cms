package com.xps.fvcms.auth.entities;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MessageResponse {

    private String message;

}
