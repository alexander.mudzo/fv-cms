package com.xps.fvcms.auth.entities;


import lombok.*;

import java.util.List;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoResponse {

    private Integer id;

    private String userName;

    private String email;

    private List<String> roles;

    private String jwt;

}
