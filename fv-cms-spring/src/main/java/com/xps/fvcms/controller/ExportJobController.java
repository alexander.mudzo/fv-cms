package com.xps.fvcms.controller;

import com.xps.fvcms.model.ExportJob;
import com.xps.fvcms.model.dto.ExportJobDto;
import com.xps.fvcms.repository.WarehouseItemRepository;
import com.xps.fvcms.service.ExportJobService;
import com.xps.fvcms.utls.RestPreconditions;
import jline.internal.Preconditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class ExportJobController {

    @Autowired
    private ExportJobService service;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private WarehouseItemRepository warehouseItemRepository;

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/exportjobs")
    public List<ExportJobDto> findAll() {
        final List<ExportJob> items = service.findAll();
        return items.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/exportjobs/{id}")
    public ExportJobDto findById(@PathVariable("id") Integer id) {
        final ExportJob item = RestPreconditions.checkFound(service.findById(id));
        return convertToDto(item);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PostMapping(value = "/exportjobs")
    @ResponseStatus(HttpStatus.CREATED)
    public ExportJobDto create(@RequestBody ExportJobDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        return convertToDto(service.addJob(convertToEntity(resource)));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PutMapping(value = "/exportjobs/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ExportJobDto update(@PathVariable( "id" ) Integer id, @RequestBody ExportJobDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        Preconditions.checkNotNull(service.findById(id));
        return convertToDto(service.updateJob(id, convertToEntity(resource)));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/exportjobs/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Integer id) {
        service.deleteJobById(id);
    }


    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/exportjobs")
    @ResponseStatus(HttpStatus.OK)
    public void deleteByIds(@RequestParam("ids") List<Integer> ids) {
        service.deleteJobsByIds(ids);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PatchMapping(value = "/exportjobs/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ExportJobDto updateState(@PathVariable( "id" ) Integer id, @RequestBody ExportJobDto resource) {
        Preconditions.checkNotNull(resource);
        Preconditions.checkNotNull(service.findById(id));
        return convertToDto(service.updateState(id, convertToEntity(resource)));
    }

    private ExportJobDto convertToDto(ExportJob exportJob) {
        return modelMapper.map(exportJob, ExportJobDto.class);
    }

    private ExportJob convertToEntity(ExportJobDto dto) {
        return modelMapper.map(dto, ExportJob.class);
    }



}
