package com.xps.fvcms.controller;


import com.xps.fvcms.exceptions.MyResourceNotFoundException;
import com.xps.fvcms.model.Category;
import com.xps.fvcms.model.dto.CategoryDto;
import com.xps.fvcms.repository.WarehouseItemRepository;
import com.xps.fvcms.service.CategoryService;
import com.xps.fvcms.utls.RestPreconditions;
import jline.internal.Preconditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class CategoryController {


    @Autowired
    private CategoryService service;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private WarehouseItemRepository warehouseItemRepository;

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/categories")
    public List<CategoryDto> findAll() {
        final List<Category> items = service.findAll();
        return items.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/categories/{id}")
    public CategoryDto findById(@PathVariable("id") Integer id) {
        final Category item = RestPreconditions.checkFound(service.findById(id));
        return convertToDto(item);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PostMapping(value = "/categories")
    @ResponseStatus(HttpStatus.CREATED)
    public CategoryDto create(@RequestBody CategoryDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        return convertToDto(service.addCategory(convertToEntity(resource)));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PutMapping(value = "/categories/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryDto update(@PathVariable( "id" ) Integer id, @RequestBody CategoryDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        Preconditions.checkNotNull(service.findById(id));
        return convertToDto(service.updateCategory(id, convertToEntity(resource)));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/categories/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Integer id) {
        service.deleteCategoryById(id);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/categories")
    @ResponseStatus(HttpStatus.OK)
    public void deleteByIds(@RequestParam("ids") List<Integer> ids) {
        service.deleteCategoriesByIds(ids);
    }

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping("/warehouseitems/{itemId}/categories")
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryDto> getAllCategoriesByItemId(@PathVariable(value = "itemId") UUID itemId) {
        if (!warehouseItemRepository.existsById(itemId)) {
            throw new MyResourceNotFoundException("Not found Item with id = " + itemId);
        }

        return service.findCategoriesByItemsId(itemId).stream().map(this::convertToDto).collect(Collectors.toList());
    }


    private CategoryDto convertToDto(Category category) {
        return modelMapper.map(category, CategoryDto.class);
    }

    private Category convertToEntity(CategoryDto categoryDto) {
        return modelMapper.map(categoryDto, Category.class);
    }

}
