package com.xps.fvcms.controller;


import com.xps.fvcms.model.Order;
import com.xps.fvcms.model.OrderAudit;
import com.xps.fvcms.model.dto.OrderDto;
import com.xps.fvcms.repository.SupplierRepository;
import com.xps.fvcms.service.OrderService;
import com.xps.fvcms.utls.RestPreconditions;
import jline.internal.Preconditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class OrderController {

    @Autowired
    private OrderService service;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SupplierRepository supplierRepository;


    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/orders")
    public List<OrderDto> findAll() {
        final List<Order> items = service.findAll();
        return items.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/orders/{id}")
    public OrderDto findById(@PathVariable("id") Integer id) {
        final Order item = RestPreconditions.checkFound(service.findById(id));
        return convertToDto(item);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PostMapping(value = "/orders")
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDto create(@RequestBody OrderDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);

        Order order = convertToEntity(resource);

        return convertToDto(service.addItem(order));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PutMapping(value = "/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDto update(@PathVariable( "id" ) Integer id, @RequestBody OrderDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        Preconditions.checkNotNull(service.findById(id));

        Order order = convertToEntity(resource);

        return convertToDto(service.updateItem(id, order));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Integer id) {
        service.deleteItemById(id);
    }


    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/orders")
    @ResponseStatus(HttpStatus.OK)
    public void deleteByIds(@RequestParam("ids") List<Integer> ids) {
        service.deleteItemsByIds(ids);
    }


    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PatchMapping(value = "/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDto updateState(@PathVariable( "id" ) Integer id, @RequestBody OrderDto resource) {
        Preconditions.checkNotNull(resource);
        Preconditions.checkNotNull(service.findById(id));

        Order order = convertToEntity(resource);

        return convertToDto(service.updateState(id, order));
    }

    private OrderDto convertToDto(Order order) {
        return modelMapper.map(order, OrderDto.class);
    }

    private Order convertToEntity(OrderDto orderDto) {
        return modelMapper.map(orderDto, Order.class);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/orders/audits")
    public List<OrderAudit> findAllAudits() {
        return service.findAllAudits();
    }
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/orders/{id}/audits")
    public List<OrderAudit> findAuditsByOrderId(@PathVariable("id") Integer id) {
        return service.findAuditsByOrderId(id);
    }

}
