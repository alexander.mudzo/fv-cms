package com.xps.fvcms.controller;


import com.xps.fvcms.model.Supplier;
import com.xps.fvcms.model.dto.SupplierDto;
import com.xps.fvcms.repository.OrderRepository;
import com.xps.fvcms.service.SupplierService;
import com.xps.fvcms.utls.RestPreconditions;
import jline.internal.Preconditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class SupplierController {
    @Autowired
    private SupplierService service;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private OrderRepository orderRepository;


    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/suppliers")
    public List<SupplierDto> findAll() {
        final List<Supplier> items = service.findAll();
        return items.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/suppliers/{id}")
    public SupplierDto findById(@PathVariable("id") UUID id) {
        final Supplier item = RestPreconditions.checkFound(service.findById(id));
        return convertToDto(item);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PostMapping(value = "/suppliers")
    @ResponseStatus(HttpStatus.CREATED)
    public SupplierDto create(@RequestBody SupplierDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        return convertToDto(service.addItem(convertToEntity(resource)));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PutMapping(value = "/suppliers/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SupplierDto update(@PathVariable( "id" ) UUID id, @RequestBody SupplierDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        Preconditions.checkNotNull(service.findById(id));
        return convertToDto(service.updateItem(id, convertToEntity(resource)));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/suppliers/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") UUID id) {
        service.deleteItemById(id);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/suppliers")
    @ResponseStatus(HttpStatus.OK)
    public void deleteByIds(@RequestParam("ids") List<UUID> ids) {
        service.deleteItemsByIds(ids);
    }

    private SupplierDto convertToDto(Supplier supplier) {
        return modelMapper.map(supplier, SupplierDto.class);
    }

    private Supplier convertToEntity(SupplierDto supplierDto) {
        return modelMapper.map(supplierDto, Supplier.class);
    }



}
