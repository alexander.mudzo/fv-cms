package com.xps.fvcms.controller;

import com.xps.fvcms.exceptions.MyResourceNotFoundException;
import com.xps.fvcms.model.WarehouseItem;
import com.xps.fvcms.model.dto.WarehouseItemDto;
import com.xps.fvcms.repository.CategoryRepository;
import com.xps.fvcms.service.WarehouseItemService;
import com.xps.fvcms.utls.RestPreconditions;
import jline.internal.Preconditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


import java.text.ParseException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class WarehouseController {

    @Autowired
    private WarehouseItemService service;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CategoryRepository categoryRepository;


    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/warehouseitems")
    public List<WarehouseItemDto> findAll() {
        final List<WarehouseItem> items = service.findAll();
        return items.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping(value = "/warehouseitems/{id}")
    public WarehouseItemDto findById(@PathVariable("id") UUID id) {
        final WarehouseItem item = RestPreconditions.checkFound(service.findById(id));
        return convertToDto(item);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PostMapping(value = "/warehouseitems")
    @ResponseStatus(HttpStatus.CREATED)
    public WarehouseItemDto create(@RequestBody WarehouseItemDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        return convertToDto(service.addItem(convertToEntity(resource)));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @PutMapping(value = "/warehouseitems/{id}")
    @ResponseStatus(HttpStatus.OK)
    public WarehouseItemDto update(@PathVariable( "id" ) UUID id, @RequestBody WarehouseItemDto resource) throws ParseException {
        Preconditions.checkNotNull(resource);
        Preconditions.checkNotNull(service.findById(id));
        return convertToDto(service.updateItem(id, convertToEntity(resource)));
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/warehouseitems/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") UUID id) {
        service.deleteItemById(id);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @DeleteMapping(value = "/warehouseitems")
    @ResponseStatus(HttpStatus.OK)
    public void deleteByIds(@RequestParam("ids") List<UUID> ids) {
        service.deleteItemsByIds(ids);
    }

    @PreAuthorize("hasAnyRole('ROLE_VIEWER','ROLE_USER','ROLE_ADMIN')")
    @GetMapping("/categories/{categoryId}/warehouseitems")
    public List<WarehouseItemDto> getAllItemsByCategoryId(@PathVariable(value = "categoryId") Integer categoryId) {
        if (!categoryRepository.existsById(categoryId)) {
            throw new MyResourceNotFoundException("Not found category with id = " + categoryId);
        }
        return service.findByCategoryId(categoryId).stream().map(this::convertToDto).collect(Collectors.toList());
    }

    private WarehouseItemDto convertToDto(WarehouseItem warehouseItem) {
        return modelMapper.map(warehouseItem, WarehouseItemDto.class);
    }

    private WarehouseItem convertToEntity(WarehouseItemDto warehouseItemDto) throws ParseException {
        return modelMapper.map(warehouseItemDto, WarehouseItem.class);
    }

}
