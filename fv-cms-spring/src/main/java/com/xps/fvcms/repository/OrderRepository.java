package com.xps.fvcms.repository;

import com.xps.fvcms.model.Order;
import com.xps.fvcms.repository.custom.CustomOrderRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> , CustomOrderRepository {

}
