package com.xps.fvcms.repository.custom.impl;

import com.xps.fvcms.model.Order;
import com.xps.fvcms.model.Supplier;
import com.xps.fvcms.repository.custom.CustomSupplierRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

public class CustomSupplierRepositoryImpl implements CustomSupplierRepository {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public void cascadeDeleteById(UUID id) {
        // Retrieve the movie with this ID
        Supplier managedEntry = em.find(Supplier.class, id);
        if (managedEntry != null) {
            try {
                // Removed supplier from all referenced orders
                List.copyOf(managedEntry.getOrders()).forEach(Order::removeSupplier);

                // Now remove the item
                em.remove(managedEntry);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    @Override
    public Supplier update(UUID id, Supplier supplier) {

        Supplier managedEntry = em.find(Supplier.class, id);
        managedEntry.setName(supplier.getName());
        managedEntry.setUpdatedAt(supplier.getUpdatedAt());

        em.persist(managedEntry);

        return managedEntry;
    }

    @Transactional
    @Override
    public void deleteByIds(List<UUID> ids) {
        ids.forEach(this::cascadeDeleteById);
    }
}
