package com.xps.fvcms.repository.custom;

import com.xps.fvcms.model.Order;

import java.util.List;

public interface CustomOrderRepository {
    Order update(Integer id, Order item);
    Order create(Order order);
    void cascadeDeleteById(Integer id);
    Order updateState(Integer id, Order order);
    void deleteByIds(List<Integer> ids);
}
