package com.xps.fvcms.repository.custom.impl;

import com.xps.fvcms.audit.Auditor;
import com.xps.fvcms.exceptions.MyResourceNotFoundException;
import com.xps.fvcms.model.*;
import com.xps.fvcms.repository.custom.CustomExportJobRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

public class CustomExportJobRepositoryImpl implements CustomExportJobRepository {


    @PersistenceContext
    private EntityManager em;

    @Autowired
    Auditor auditor;

    @Transactional
    @Override
    public void cascadeDeleteById(Integer id) {
        // Retrieve the movie with this ID
        ExportJob item = em.find(ExportJob.class, id);
        if (item != null) {
            try {

                // Remove all references to this item by WarehouseItemExportJob links
                List.copyOf(item.getItems()).forEach(link -> {
                    link.getItem().removeJob(link);
                    link.getJob().removeItem(link);
                    em.remove(link);
                });

                // Now remove the item
                em.remove(item);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    @Override
    public ExportJob update(Integer id, ExportJob job) {

        ExportJob item = em.find(ExportJob.class, id);

        item.setDescription(job.getDescription());
        item.setName(job.getName());
        item.setDeadline(job.getDeadline());

        item.setUpdatedAt(job.getUpdatedAt());

        for (WarehouseItemExportJob warehouseItemExportJob : List.copyOf(item.getItems())) {
            if(warehouseItemExportJob.getId()!=null) {

                WarehouseItemExportJob tmp = em.find(WarehouseItemExportJob.class, warehouseItemExportJob.getId());

                tmp.getItem().removeJob(tmp);
                tmp.getJob().removeItem(tmp);
                em.remove(tmp);
            }
        }

        for (WarehouseItemExportJob warehouseItemExportJob : job.getItems()) {

            WarehouseItem tmp = em.find(WarehouseItem.class, warehouseItemExportJob.getItem().getId());

            if(tmp==null) {
                throw new MyResourceNotFoundException("Warehouse item not found with id: " + warehouseItemExportJob.getId());
            }
                WarehouseItemExportJob link = new WarehouseItemExportJob();

                link.setCount(warehouseItemExportJob.getCount());
                link.setJob(item);
                link.setItem(tmp);
                link.setMissingCount(0); //TODO current missing count is reset  what to do ???
                link.setAutomaticOrderCreated(false);

                tmp.addJob(link);

                em.persist(link);

                item.addItem(link);

        }

        em.persist(item);

        return item;
    }

    @Transactional
    @Override
    public ExportJob create(ExportJob job) {

        // Create new item and set its direct props
        ExportJob item = new ExportJob();
        item.setDescription(job.getDescription());
        item.setName(job.getName());
        item.setCreatedAt(job.getCreatedAt());
        item.setDeadline(job.getDeadline());
        item.setStatus(job.getStatus());

        // Look for warehouse item ids and initialize linking entity
        for (WarehouseItemExportJob warehouseItemExportJob : job.getItems()) {
            if(warehouseItemExportJob.getItem().getId()!=null) {
                WarehouseItem tmp = em.find(WarehouseItem.class, warehouseItemExportJob.getItem().getId());

                if(tmp==null) {
                    throw new MyResourceNotFoundException("Warehouse item not found with id: " + warehouseItemExportJob.getId());
                }

                WarehouseItemExportJob link = new WarehouseItemExportJob();

                link.setJob(item);
                link.setItem(tmp);
                link.setCount(warehouseItemExportJob.getCount());
                link.setMissingCount(0);
                link.setAutomaticOrderCreated(false);
                tmp.addJob(link);
                em.persist(link);

                item.addItem(link);

            }
            //TODO Throw some kind of Exception for incorrect body
        }

        em.persist(item);

        return item;
    }

    @Transactional
    @Override
    public ExportJob updateState(Integer id, ExportJob job) {

        ExportJob item = em.find(ExportJob.class, id);

        switch (job.getStatus()) {
            case IN_PROGRESS:
                onJobStarted(item);
                break;
            case READY:
                onJobReady(item);
                break;
            case DONE:
                if(item.getStatus() == JobStatus.READY) {
                    item.setStatus(JobStatus.DONE);
                    em.persist(item);
                }
                break;
            case CANCELED:
                onJobCanceled(item);
                break;
        }

        return item;
    }
    @Transactional
    @Override
    public void deleteByIds(List<Integer> ids) {
        ids.forEach(this::cascadeDeleteById);
    }

    private void onJobStarted(ExportJob item) {
        if(item.getStatus() == JobStatus.CREATED || item.getStatus() == JobStatus.CANCELED) {

            boolean hasAllRequiredItems = true;

            // Job started allocate amount of items
            for(WarehouseItemExportJob link : item.getItems()) {

                WarehouseItem warehouseItem = em.find(WarehouseItem.class, link.getItem().getId());
                final int toSubtract = link.getCount();
                final int initialCount = warehouseItem.getCount();

                final int res = initialCount - toSubtract;

                if(res>=0) {
                    warehouseItem.setCount(res);
                    link.setMissingCount(0);

                }
                else {
                    warehouseItem.setCount(0);
                    link.setMissingCount(-1 * res);
                    hasAllRequiredItems = false;
                }

                em.persist(link);
                em.persist(warehouseItem);
            }

            item.setStatus(hasAllRequiredItems ? JobStatus.IN_PROGRESS : JobStatus.BLOCKED);
            em.persist(item);

        }
    }

    private void onJobCanceled(ExportJob item) {
        if(item.getStatus() != JobStatus.CANCELED || item.getStatus() != JobStatus.DONE) {

            // Job started allocate amount of items
            for(WarehouseItemExportJob link : item.getItems()) {

                WarehouseItem warehouseItem = em.find(WarehouseItem.class, link.getItem().getId());
                final int toAdd = link.getCount();
                final int initialCount = warehouseItem.getCount();

                final int res = initialCount + toAdd;

                warehouseItem.setCount(res);
                link.setMissingCount(0);

                em.persist(link);
                em.persist(warehouseItem);
            }

            item.setStatus(JobStatus.CANCELED);
            em.persist(item);

        }
    }

    private void onJobReady(ExportJob item) {

        if(item.getStatus() == JobStatus.IN_PROGRESS) {
            item.setStatus(JobStatus.READY);
            em.persist(item);
        }

        else if(item.getStatus() == JobStatus.BLOCKED) {

            for(WarehouseItemExportJob link : item.getItems()) {

                WarehouseItem warehouseItem = em.find(WarehouseItem.class, link.getItem().getId());

                final int missingToOrder = link.getMissingCount();
                final int initialCount = warehouseItem.getCount();

                final int res = missingToOrder - initialCount;

                if(res>0) {

                    if(!link.getAutomaticOrderCreated()) {

                        auditor.setCurrentLoggedUser(em);

                        Order automaticOrder = new Order();

                        automaticOrder.setStatus(OrderStatus.CREATED);
                        automaticOrder.setCreatedAt(LocalDateTime.now());


                        WarehouseItemOrder link2 = new WarehouseItemOrder();

                        link2.setOrder(automaticOrder);
                        link2.setItem(warehouseItem);
                        link2.setCount(res);

                        em.persist(link2);

                        automaticOrder.addItem(link2);

                        link.setAutomaticOrderCreated(true);
                    }

                } else {

                    final int remaining = warehouseItem.getCount() - link.getMissingCount();

                    warehouseItem.setCount(remaining);
                            link.setMissingCount(0);

                    item.setStatus(JobStatus.IN_PROGRESS);
                }

                em.persist(link);
                em.persist(warehouseItem);
            }

            em.persist(item);
        }
    }


}
