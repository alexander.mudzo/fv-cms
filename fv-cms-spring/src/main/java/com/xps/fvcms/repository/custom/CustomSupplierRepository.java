package com.xps.fvcms.repository.custom;

import com.xps.fvcms.model.Supplier;

import java.util.List;
import java.util.UUID;

public interface CustomSupplierRepository {
    void cascadeDeleteById(UUID id);
    Supplier update(UUID id, Supplier item);

    void deleteByIds(List<UUID> ids);
}
