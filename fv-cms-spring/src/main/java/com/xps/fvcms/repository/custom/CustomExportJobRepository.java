package com.xps.fvcms.repository.custom;

import com.xps.fvcms.model.ExportJob;

import java.util.List;

public interface CustomExportJobRepository {
    void cascadeDeleteById(Integer id);
    ExportJob update(Integer id, ExportJob job);
    ExportJob create(ExportJob job);
    ExportJob updateState(Integer id, ExportJob job);
    void deleteByIds(List<Integer> ids);
}
