package com.xps.fvcms.repository;

import com.xps.fvcms.model.OrderAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderAuditRepository extends JpaRepository<OrderAudit, Integer> {

}
