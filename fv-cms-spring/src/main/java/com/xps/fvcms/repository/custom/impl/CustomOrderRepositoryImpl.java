package com.xps.fvcms.repository.custom.impl;

import com.xps.fvcms.audit.Auditor;
import com.xps.fvcms.exceptions.MyResourceNotFoundException;
import com.xps.fvcms.model.*;
import com.xps.fvcms.repository.custom.CustomOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

public class CustomOrderRepositoryImpl implements CustomOrderRepository {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    Auditor auditor;

    @Transactional
    @Override
    public Order create(Order order) {
        auditor.setCurrentLoggedUser(em);

        Order managedEntry = new Order();
        managedEntry.setStatus(order.getStatus());
        managedEntry.setCreatedAt(order.getCreatedAt());
        managedEntry.setStatus(order.getStatus());

        if(order.getSupplier()!= null && order.getSupplier().getId() != null) {
            Supplier tmp = em.find(Supplier.class, order.getSupplier().getId());

            if(tmp==null) {
                throw new MyResourceNotFoundException("Supplier not found with id: " + order.getSupplier().getId());
            }

            managedEntry.setSupplier(tmp);
        }

        for (WarehouseItemOrder warehouseItemOrder : order.getItems()) {
            if(warehouseItemOrder.getItem().getId()!=null) {
                WarehouseItem tmp = em.find(WarehouseItem.class, warehouseItemOrder.getItem().getId());

                if(tmp==null) {
                    throw new MyResourceNotFoundException("Warehouse item not found with id: " + warehouseItemOrder.getId());
                }

                WarehouseItemOrder link = new WarehouseItemOrder();

                link.setOrder(managedEntry);
                link.setItem(tmp);
                link.setCount(warehouseItemOrder.getCount());
                tmp.addOrder(link);

                em.persist(link);

                managedEntry.addItem(link);

            }
            else if(warehouseItemOrder.getItem().getName()!=null) {

                WarehouseItem tmp = new WarehouseItem();
                tmp.setName(warehouseItemOrder.getItem().getName());
                tmp.setCreatedAt(LocalDateTime.now());
                em.persist(tmp);

                WarehouseItemOrder link = new WarehouseItemOrder();

                link.setOrder(managedEntry);
                link.setItem(tmp);
                link.setCount(warehouseItemOrder.getCount());

                tmp.addOrder(link);

                em.persist(link);

                managedEntry.addItem(link);
            }

            //TODO Throw some kind of Exception for incorrect body
        }

        em.persist(managedEntry);

        return managedEntry;
    }

    @Transactional
    @Override
    public Order update(Integer id, Order order) {
        auditor.setCurrentLoggedUser(em);

        Order managedEntry = em.find(Order.class, id);

        managedEntry.setUpdatedAt(order.getUpdatedAt());

        if(order.getSupplier()!=null && order.getSupplier().getId() !=null) {
            Supplier tmp = em.find(Supplier.class, order.getSupplier().getId());
            if(tmp==null) {
                throw new MyResourceNotFoundException("Supplier not found with id: " + order.getSupplier().getId());
            }
            managedEntry.setSupplier(tmp);
        }
        else  {
            managedEntry.removeSupplier();
        }


        for(WarehouseItemOrder warehouseItemOrder : List.copyOf(managedEntry.getItems())) {

            WarehouseItemOrder tmp = em.find(WarehouseItemOrder.class, warehouseItemOrder.getId());

            tmp.getOrder().removeItem(warehouseItemOrder);
            tmp.getItem().removeOrder(warehouseItemOrder);

            em.remove(tmp);

        }

        for (WarehouseItemOrder warehouseItemOrder : order.getItems()) {
            WarehouseItem tmp = em.find(WarehouseItem.class, warehouseItemOrder.getItem().getId());
            if(tmp==null) {
                throw new MyResourceNotFoundException("Warehouse item not found with id: " + warehouseItemOrder.getId());
            }
            WarehouseItemOrder link = new WarehouseItemOrder();

            link.setOrder(managedEntry);
            link.setItem(tmp);
            link.setCount(warehouseItemOrder.getCount());

            tmp.addOrder(link);

            em.persist(link);

            managedEntry.addItem(link);
        }

        em.persist(managedEntry);

        return managedEntry;
    }


    @Transactional
    @Override
    public void cascadeDeleteById(Integer id) {
        auditor.setCurrentLoggedUser(em);

        // Retrieve the order with this ID
        Order item = em.find(Order.class, id);
        if (item != null) {
            try {
                // Removed order from referenced supplier
                if(item.getSupplier()!=null && item.getSupplier().getId()!=null) {
                    Supplier supplier = em.find(Supplier.class, item.getSupplier().getId());
                    supplier.removeOrder(item);
                    em.persist(supplier);
                }

                List.copyOf(item.getItems()).forEach(itm -> {
                    itm.getItem().removeOrder(itm);
                    itm.getOrder().removeItem(itm);
                    em.remove(itm);
                });

                // Now remove the item
                em.remove(item);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    @Override
    public Order updateState(Integer id, Order order) {
        auditor.setCurrentLoggedUser(em);

        Order item = em.find(Order.class, id);

        switch (order.getStatus()) {

            case CREATED:

                break;
            case PENDING:
                if(item.getStatus() == OrderStatus.CREATED) {
                    item.setStatus(OrderStatus.PENDING);
                    em.persist(item);
                }
                break;
            case ACCEPTED:
                if(item.getStatus() == OrderStatus.PENDING) {
                    item.setStatus(OrderStatus.ACCEPTED);
                    em.persist(item);
                }
                break;
            case IN_PROGRESS:
                if(item.getStatus() == OrderStatus.ACCEPTED) {
                    item.setStatus(OrderStatus.IN_PROGRESS);
                    em.persist(item);
                }
                break;
            case REJECTED:
                if(item.getStatus() != OrderStatus.DONE) {
                    item.setStatus(OrderStatus.REJECTED);
                    em.persist(item);
                }
                break;
            case DONE:
                onDone(item);
                break;
        }

        return item;
    }
    @Transactional
    @Override
    public void deleteByIds(List<Integer> ids) {
        ids.forEach(this::cascadeDeleteById);
    }


    private void onDone(Order item){

        if(item.getStatus() == OrderStatus.IN_PROGRESS) {

            // Job started allocate amount of items
            for(WarehouseItemOrder link : item.getItems()) {

                WarehouseItem warehouseItem = em.find(WarehouseItem.class, link.getItem().getId());
                final int toAdd = link.getCount();
                final int initialCount = warehouseItem.getCount();

                final int res = initialCount + toAdd;

                warehouseItem.setCount(res);

                em.persist(warehouseItem);
            }

            item.setStatus(OrderStatus.DONE);
            em.persist(item);

        }

    }


}
