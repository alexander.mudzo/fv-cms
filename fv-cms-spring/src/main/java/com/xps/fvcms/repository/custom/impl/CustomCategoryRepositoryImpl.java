package com.xps.fvcms.repository.custom.impl;

import com.xps.fvcms.model.Category;
import com.xps.fvcms.repository.custom.CustomCategoryRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


public class CustomCategoryRepositoryImpl implements CustomCategoryRepository {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public Category update(Integer id, Category category) {

        Category item = em.find(Category.class, id);
        item.setName(category.getName());
        item.setUpdatedAt(category.getUpdatedAt());
        em.persist(item);

        return item;
    }

    @Transactional
    @Override
    public void cascadeDeleteById(Integer id) {
        // Retrieve the category with this ID
        Category category = em.find(Category.class, id);
        if (category != null) {
            try {

                // Remove all references to this category in its items
                category.getItems().forEach(item -> item.removeCategory(category));

                // Now remove the category
                em.remove(category);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deleteByIds(List<Integer> ids) {
        ids.forEach(this::cascadeDeleteById);
    }
}
