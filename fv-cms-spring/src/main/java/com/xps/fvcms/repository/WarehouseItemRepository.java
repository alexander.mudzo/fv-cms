package com.xps.fvcms.repository;

import com.xps.fvcms.model.WarehouseItem;
import com.xps.fvcms.repository.custom.CustomWarehouseItemRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface WarehouseItemRepository extends JpaRepository<WarehouseItem, UUID>, CustomWarehouseItemRepository {

    List<WarehouseItem> findAllByName(String name);

    List<WarehouseItem> findAllByCategories_Name(String categoryName);

    List<WarehouseItem> findWarehouseItemsByCategoriesId(Integer categoryId);

}
