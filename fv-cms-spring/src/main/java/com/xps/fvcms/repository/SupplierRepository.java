package com.xps.fvcms.repository;

import com.xps.fvcms.model.Supplier;
import com.xps.fvcms.repository.custom.CustomSupplierRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SupplierRepository extends JpaRepository<Supplier, UUID>, CustomSupplierRepository {

}
