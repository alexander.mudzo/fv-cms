package com.xps.fvcms.repository.custom;

import com.xps.fvcms.model.WarehouseItem;

import java.util.List;
import java.util.UUID;

public interface CustomWarehouseItemRepository {

    void cascadeDeleteById(UUID id);
    void deleteByIds(List<UUID> ids);
    WarehouseItem update(UUID id, WarehouseItem warehouseItem);
    WarehouseItem create(WarehouseItem warehouseItem);
}
