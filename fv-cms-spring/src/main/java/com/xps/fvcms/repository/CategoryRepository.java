package com.xps.fvcms.repository;

import com.xps.fvcms.model.Category;
import com.xps.fvcms.repository.custom.CustomCategoryRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>, CustomCategoryRepository {

    List<Category> findCategoriesByItemsId(UUID id);

    Category findByName(String name);

}
