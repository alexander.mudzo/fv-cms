package com.xps.fvcms.repository.custom;

import com.xps.fvcms.model.Category;

import java.util.List;

public interface CustomCategoryRepository {
    void cascadeDeleteById(Integer id);
    Category update(Integer id, Category category);
    void deleteByIds(List<Integer> ids);
}
