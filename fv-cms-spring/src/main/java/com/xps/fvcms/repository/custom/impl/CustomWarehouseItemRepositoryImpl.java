package com.xps.fvcms.repository.custom.impl;

import com.xps.fvcms.exceptions.MyResourceNotFoundException;
import com.xps.fvcms.model.Category;
import com.xps.fvcms.model.WarehouseItem;
import com.xps.fvcms.repository.custom.CustomWarehouseItemRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

public class CustomWarehouseItemRepositoryImpl implements CustomWarehouseItemRepository {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public WarehouseItem create(WarehouseItem warehouseItem) {

        WarehouseItem item = new WarehouseItem();
        item.setCount(warehouseItem.getCount());
        item.setName(warehouseItem.getName());
        item.setCreatedAt(warehouseItem.getCreatedAt());

        for (Category cat : warehouseItem.getCategories()) {
            if(cat.getId()!=null) {
                Category tmp = em.find(Category.class, cat.getId());
                tmp.addItem(item);
                item.addCategory(tmp);
            }
            else if(cat.getName()!=null) {
                Category tmp = new Category();
                tmp.setCreatedAt(LocalDateTime.now());
                tmp.setName(cat.getName());
                em.persist(tmp);
                tmp.addItem(item);
                item.addCategory(tmp);
            }

        }

        em.persist(item);

        return item;
    }

    @Transactional
    @Override
    public WarehouseItem update(UUID id, WarehouseItem warehouseItem) {


        WarehouseItem item = em.find(WarehouseItem.class, id);
        item.setCount(warehouseItem.getCount());
        item.setName(warehouseItem.getName());
        item.setUpdatedAt(warehouseItem.getUpdatedAt());

        item.removeAllCategories();

        for (Category cat : warehouseItem.getCategories()) {
            Category tmp = em.find(Category.class, cat.getId());
            if(tmp==null) {
                throw new MyResourceNotFoundException("Category not found id: " + cat.getId());
            }
            tmp.addItem(item);
            item.addCategory(tmp);
        }

        em.persist(item);

        return item;
    }

    @Transactional
    @Override
    public void cascadeDeleteById(UUID id) {
        // Retrieve the movie with this ID
        WarehouseItem item = em.find(WarehouseItem.class, id);
        if (item != null) {
            try {

                // Remove all references to this item by categories
                item.getCategories().forEach(category -> category.removeItem(item));

                item.getOrders().forEach(link -> {
                    link.getOrder().removeItem(link);
                    link.getItem().removeOrder(link);
                    em.remove(link);
                });

                item.getJobs().forEach(link -> {
                    link.getJob().removeItem(link);
                    link.getItem().removeJob(link);
                    em.remove(link);
                });

                // Now remove the item
                em.remove(item);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    public void deleteByIds(List<UUID> ids) {
        ids.forEach(this::cascadeDeleteById);
    }

}
