package com.xps.fvcms.repository;

import com.xps.fvcms.model.ExportJob;
import com.xps.fvcms.repository.custom.CustomExportJobRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExportJobRepository extends JpaRepository<ExportJob, Integer>, CustomExportJobRepository {

}
