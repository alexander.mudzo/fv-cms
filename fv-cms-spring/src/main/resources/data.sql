-- ROLES SECTION START --
INSERT INTO roles (role_id, role)
VALUES(1,'ROLE_ADMIN')
ON CONFLICT (role_id)
DO
   UPDATE SET role = 'ROLE_ADMIN';

INSERT INTO roles (role_id, role)
VALUES(2,'ROLE_USER')
ON CONFLICT (role_id)
DO
   UPDATE SET role = 'ROLE_USER';

INSERT INTO roles (role_id, role)
VALUES(3,'ROLE_VIEWER')
ON CONFLICT (role_id)
DO
   UPDATE SET role = 'ROLE_VIEWER';
-- ROLES SECITON END --


-- -- Create this table only once
-- CREATE TABLE IF NOT EXISTS orders_audit_log (
--     order_id bigint NOT NULL,
--     old_row_data jsonb,
--     new_row_data jsonb,
--     dml_type dml_type NOT NULL,
--     dml_timestamp timestamp NOT NULL,
--     dml_created_by varchar(255) NOT NULL,
--     PRIMARY KEY (order_id, dml_type, dml_timestamp)
-- );

-- Drop function, cascade trigger and then recreate
DROP FUNCTION IF EXISTS orders_audit_trigger_func cascade;
CREATE FUNCTION orders_audit_trigger_func()
RETURNS trigger AS '
BEGIN
   if (TG_OP = ''INSERT'') then
       INSERT INTO orders_audit_log (
           order_id,
           old_row_data,
           new_row_data,
           dml_type,
           dml_timestamp,
           dml_created_by
       )
       VALUES(
           NEW.order_id,
           null,
           to_jsonb(NEW),
           ''INSERT'',
           CURRENT_TIMESTAMP,
           current_setting(''var.logged_user'')
       );

       RETURN NEW;
   elsif (TG_OP = ''UPDATE'') then
       INSERT INTO orders_audit_log (
           order_id,
           old_row_data,
           new_row_data,
           dml_type,
           dml_timestamp,
           dml_created_by
       )
       VALUES(
           NEW.order_id,
           to_jsonb(OLD),
           to_jsonb(NEW),
           ''UPDATE'',
           CURRENT_TIMESTAMP,
           current_setting(''var.logged_user'')
       );

       RETURN NEW;
   elsif (TG_OP = ''DELETE'') then
       INSERT INTO orders_audit_log (
           order_id,
           old_row_data,
           new_row_data,
           dml_type,
           dml_timestamp,
           dml_created_by
       )
       VALUES(
           OLD.order_id,
           to_jsonb(OLD),
           null,
           ''DELETE'',
           CURRENT_TIMESTAMP,
           current_setting(''var.logged_user'')
       );

       RETURN OLD;
   end if;

END;
'
LANGUAGE plpgsql;

CREATE TRIGGER orders_audit_trigger
AFTER INSERT OR UPDATE OR DELETE ON orders
FOR EACH ROW EXECUTE FUNCTION orders_audit_trigger_func();
